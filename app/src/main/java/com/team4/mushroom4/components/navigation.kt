package com.team4.mushroom4.components

import AddMushroomScreen
import LoginScreen
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.team4.mushroom4.R
import com.team4.mushroom4.screens.learn.quiz.RunQuizScreen
import com.team4.mushroom4.ui.theme.Mushroom4Theme
import com.team4.mushroom4.screens.menu.EatScreen.EatScreen
import com.team4.mushroom4.screens.menu.LearnScreen
import com.team4.mushroom4.screens.menu.map.MapScreen
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Popup
import androidx.compose.ui.window.PopupProperties
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.team4.mushroom4.screens.learn.rank.RankScreen
import com.team4.mushroom4.screens.ApiOpenWheather.WeatherEntryPoint
import com.team4.mushroom4.screens.bonds.BondsModel
import com.team4.mushroom4.screens.bonds.userList.UserModel
import com.team4.mushroom4.screens.forumScreen.ForumScreen
import com.team4.mushroom4.screens.login.RegisterScreen
import com.team4.mushroom4.screens.menu.RestScreen
import com.team4.mushroom4.screens.userMushrooms.MushroomCrudScreen
import com.team4.mushroom4.screens.userMushrooms.components.EditMushroomScreen


object Navigation {

    @RequiresApi(Build.VERSION_CODES.O)
    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun NavHost(navController: NavHostController) {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.destination?.route

        val hideTopBarAndFabRoutes = listOf("loginScreen", "register")

        val showTopBarAndFab = currentRoute !in hideTopBarAndFabRoutes

        Mushroom4Theme {
            Surface(
                modifier = Modifier.fillMaxSize(),
                color = Color(0xFFD2B48C)
            ) {
                Box(modifier = Modifier.fillMaxSize()) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .align(Alignment.TopCenter)
                    ) {
                        if (showTopBarAndFab) {
                            CustomTopBar(navController)
                        }

                        NavHost(
                            navController = navController,
                            startDestination = "loginScreen"
                        ) {
                            composable("buttonsContent") {
                                ButtonsContent(navController)
                            }
                            composable("map") {
                                MapScreen(navController)
                            }
                            composable("myMushrooms") {
                                MushroomCrudScreen(navController)
                            }
                            composable("eat") {
                                EatScreen(navController)
                            }
                            composable("learn") {
                                LearnScreen(navController)
                            }
                            composable("loginScreen") {
                                LoginScreen(navController)
                            }
                            composable("register") {
                                RegisterScreen(navController)
                            }
                            composable("RunQuizScreen") {
                                val runningQuiz = RunQuizScreen(navController)
                                runningQuiz.QuizScreen()
                            }
                            composable("ranking") {
                                RankScreen(navController)
                            }
                            composable("messages") {
                                ForumScreen(navController)
                            }
                            composable("restaurants") {
                                RestScreen(navController)
                            }
                            composable("addMushroomScreen") {
                                AddMushroomScreen(navController)
                            }
                            composable("editMushroomScreen/{userId}/{mushroomId}") { backStackEntry ->
                                val userId = backStackEntry.arguments?.getString("userId") ?: ""
                                val mushroomId =
                                    backStackEntry.arguments?.getString("mushroomId") ?: ""
                                EditMushroomScreen(navController, userId, mushroomId)
                            }
                            composable("weather") {
                                WeatherEntryPoint(navController)
                            }
                            composable("friendsList") {
                                val bondModel: BondsModel = BondsModel(navController)
                                bondModel.FriendList()
                            }
                            composable("usersList") {
                                val userModel: UserModel = UserModel(navController)
                                userModel.UserList()
                            }
                        }
                    }

                    if (showTopBarAndFab) {
                        FloatingActionButton(
                            onClick = {
                                navController.navigate("addMushroomScreen")
                            },
                            modifier = Modifier
                                .align(Alignment.BottomEnd)
                                .padding(16.dp)
                        ) {
                            Icon(
                                imageVector = Icons.Default.Add,
                                contentDescription = "Add Mushroom"
                            )
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun CustomTopBar(navController: NavController) {
        val AppBarHeight = 56.dp
        var expanded by remember { mutableStateOf(false) }

        val menuOptions = listOf(
            "Mapa" to "map",
            "Mis hongos" to "myMushrooms",
            "Comer" to "eat",
            "Aprender" to "learn",
            "Ranking" to "ranking",
            "Mensajes" to "messages",
            "Restaurantes" to "restaurants",
            "Clima" to "weather"
        )

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(color = Color(0xFF556B2F)),
            verticalAlignment = Alignment.CenterVertically
        ) {
            // Botón para mostrar/ocultar el menú desplegable
            IconButton(
                onClick = { expanded = !expanded },
                modifier = Modifier.size(48.dp)
            ) {
                Icon(imageVector = Icons.Default.Menu, contentDescription = null)
            }

            Spacer(modifier = Modifier.width(16.dp))

            // Contenido de la barra de herramientas
            Box(
                modifier = Modifier.weight(1f)
            ) {
                // Hacer clic en la imagen para navegar al menú principal
                Image(
                    painter = painterResource(id = R.drawable.icono_aplicacion),
                    contentDescription = "Icono Aplicacion",
                    modifier = Modifier
                        .padding(end = 16.dp)
                        .align(Alignment.CenterEnd)
                        .size(48.dp)
                        .clickable {
                            navController.navigate("buttonsContent")
                            expanded = false // Cerrar el menú cuando se navega
                        }
                )
                Text(
                    text = "MushTools",
                    color = Color.White,
                    style = TextStyle(
                        fontSize = 20.sp,
                        fontWeight = FontWeight.Bold
                    ),
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1,
                    modifier = Modifier.padding(16.dp)
                )
            }
        }

        // Utilizamos la nueva DropdownMenu
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = {
                expanded = false
            }, // Cerrar el menú cuando se hace clic fuera de él
            modifier = Modifier.offset(
                x = 16.dp,
                y = 16.dp
            ), // Mueve el menú 16 píxeles hacia abajo y hacia la derecha
            menuOptions = menuOptions,
            navController = navController
        )
    }
}

@Composable
fun DropdownMenu(
    expanded: Boolean,
    onDismissRequest: () -> Unit,
    modifier: Modifier = Modifier,
    menuOptions: List<Pair<String, String>>, // Lista de opciones del menú
    navController: NavController // NavController para la navegación
) {
    // Si el menú está expandido, mostramos el menú desplegable
    if (expanded) {
        // Popup para mostrar el menú desplegable
        Popup(
            alignment = Alignment.TopStart, // Alineación en la esquina superior izquierda
            properties = PopupProperties(
                excludeFromSystemGesture = true,
            ),
            onDismissRequest = {
                onDismissRequest() // Al hacer clic fuera del menú, se cierra
            }
        ) {
            // Columna para mostrar las opciones del menú
            Column(
                modifier = modifier
                    .background(Color.White)
                    .border(1.dp, Color.Black)
                    .padding(4.dp)
            ) {
                menuOptions.forEach { (title, destination) ->
                    // Cada opción se muestra como un elemento del menú
                    Text(
                        text = title,
                        modifier = Modifier
                            .padding(vertical = 8.dp, horizontal = 16.dp)
                            .clickable {
                                navController.navigate(destination)
                                onDismissRequest() // Se cierra el menú desplegable
                            }
                    )
                }
            }
        }
    }
}