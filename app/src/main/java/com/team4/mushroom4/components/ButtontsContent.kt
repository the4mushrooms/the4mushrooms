package com.team4.mushroom4.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.team4.mushroom4.R

data class MainButton(
    val textImageButton: String,
    val drawableImageButton:Int,
    val routeImageButton:String
)

fun mainButtonList(): List<MainButton> {
    val item1:MainButton = MainButton (
        textImageButton = "Map",
        drawableImageButton = R.drawable.mapa,
        routeImageButton = "map"
    )
    val item2:MainButton = MainButton (
        textImageButton = "My Mushrooms",
        drawableImageButton = R.drawable.seta,
        routeImageButton = "myMushrooms"
    )
    val item3:MainButton = MainButton (
        textImageButton = "Learn",
        drawableImageButton = R.drawable.aprender,
        routeImageButton = "learn"
    )
    val item4:MainButton = MainButton (
        textImageButton = "Wiki Mushrooms",
        drawableImageButton = R.drawable.wikisetas,
        routeImageButton = "eat"
    )
    val item5:MainButton = MainButton (
        textImageButton = "Messages",
        drawableImageButton = R.drawable.mensajes,
        routeImageButton = "messages"
    )
    val item6:MainButton = MainButton (
        textImageButton = "Restaurants",
        drawableImageButton = R.drawable.comer,
        routeImageButton = "restaurants"
    )
    val item7:MainButton = MainButton (
        textImageButton = "Weather",
        drawableImageButton = R.drawable.weather,
        routeImageButton = "weather"
    )
    val item8:MainButton = MainButton (
        textImageButton = "Friends",
        drawableImageButton = R.drawable.no_disponible,
        routeImageButton = "friendsList"
    )
    //
    val mutableList: MutableList<MainButton> = mutableListOf<MainButton>()
    mutableList.add(item1)
    mutableList.add(item2)
    mutableList.add(item3)
    mutableList.add(item4)
    mutableList.add(item5)
    mutableList.add(item6)
    mutableList.add(item7)
    mutableList.add(item8)
    //
    return mutableList.toList()
}

@Composable
fun ButtonItem(navController:NavController, item: MainButton){
    Box(
        modifier = Modifier
            .fillMaxWidth(),
            //.padding(top = 4.dp, bottom = 4.dp),
        contentAlignment = Alignment.Center
    ){
        MyImageButton(item.textImageButton, item.drawableImageButton, imageSize = 80) {
            navController.navigate(item.routeImageButton)
        }
    }
}

@Composable
fun ButtonsContent(navController: NavController) {
    val itemsList: List<MainButton> = mainButtonList()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ){
        LazyVerticalGrid(
            columns = GridCells.Fixed(2),
            contentPadding = PaddingValues(8.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp),
            content = {
                items(itemsList) { item ->
                    ButtonItem(
                        navController = navController,
                        item = item
                    )
                }
            }
        )
    }
}