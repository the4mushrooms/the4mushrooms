package com.team4.mushroom4.Entity.Quiz

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.team4.mushroom4.Entity.Mushroom.Mushroom
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await

class DatabaseQuizUtils {
    lateinit var list : List<Question>
    private val db = FirebaseFirestore.getInstance()
    private val quizCollection: CollectionReference = db.collection("quiz")
    private val plantsCollection: CollectionReference = db.collection("plants")

    /**getQuiz
     * Restona la lista de Quiz con un lvl concreto
     */
    suspend fun getQuiz(lvl: Int, type: String): List<Question> {
        val quizList = mutableListOf<Question>()
        val snapshot = quizCollection.whereEqualTo("lvl", lvl).get().await()
        for (document in snapshot) {
            val quiz = document.toObject(Question::class.java)
            val documentPlantId = quiz.answer_id!!
            val plant = getPlantById(documentPlantId)
            val newQuiz = quiz.copy(answer_object = plant!!)
            quizList.add(newQuiz)
        }
        return quizList.toList()
    }

    /**getPlantById
     * Retorna el objeto Mushrrom mediante la id de su documento
     */
    private suspend fun getPlantById(documentId: String): Mushroom? {
        return plantsCollection.document(documentId).get().await()?.toObject(Mushroom::class.java)
    }

    /**callListOfQuiz
     * Funcion que bloquea el programa hasta acabar la cosulta de las funciones
     * añade el resultado a la variable list
     */
    fun callListOfQuiz(lvl: Int, type: String) = runBlocking {
        list = getQuiz(lvl,type)
    }
}