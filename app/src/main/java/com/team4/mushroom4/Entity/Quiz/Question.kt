package com.team4.mushroom4.Entity.Quiz

import com.team4.mushroom4.Entity.Mushroom.Mushroom

data class Question(
    var route: Int? = 0, // ruta de la lista para mostrar la screen
    val title: String = "", // titulo de la quiz
    val lvl : Int = 0, // Nivel en el que puede aparecer esta pregunta
    var question: String = "", // Pregunta
    var answer_attribute: String = "", // Atributo de la respuesta utilizado por si queremos variar a nombre, descripcion, etc
    var answer_id: String? = null, // Respuesta correcta a mostrar y puntuar
    var answer_object : Mushroom = Mushroom()
){
    lateinit var answer_options : List<String> //Opciones de respuesta, entre ellas esta al correcta
    fun quizConstructor(inputQuestion: String, inputAttribute : String, inputObject: String){
        question = inputQuestion
        answer_attribute = inputAttribute
        answer_id = inputObject
    }
    fun setAnswerOptions(lista : List<String>) {
        answer_options = lista
    }
}