package com.team4.mushroom4.Entity

data class Customer(
    var id: String = "",
    val username: String = "",
    val fullName: String = "",
    val password: String = "",
    val email: String = "",
    val phone: String = "",
    val imagePath: String = ""
) /*{

    constructor(username: String, password: String, email: String) : this(
        id = "",
        username = username,
        fullName = "",
        password = password,
        email = email,
        phone = "",
        imagePath = ""
    )

    constructor(id: String, username: String, fullName: String, password: String, email: String, phone: String, imagePath: String) : this(
        username,
        fullName,
        password,
        email,
        phone,
        imagePath
    )
}
*/

