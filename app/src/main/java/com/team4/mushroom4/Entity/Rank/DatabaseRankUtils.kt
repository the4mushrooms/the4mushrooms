package com.team4.mushroom4.Entity.Rank

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.team4.mushroom4.Entity.Customer
import com.team4.mushroom4.screens.learn.rank.components.getMonthgetDay
import kotlinx.coroutines.tasks.await

class DatabaseRankUtils {
    private val db = FirebaseFirestore.getInstance()
    private val rankCollection: CollectionReference = db.collection("ranking")
    private val userCollection: CollectionReference = db.collection("users")
    private val currentUserUid: String? = FirebaseAuth.getInstance().currentUser?.uid

    /**add
     * Guardar objeto Rank
     */
    fun add(rank: Rank) {
        rank.user_id = getUserId()
        rankCollection.add(rank)
    }

    /**getRankSnapshot
     * Return snapshot de Rank
     * */
    @RequiresApi(Build.VERSION_CODES.O)
    suspend fun getRankSnapshot(filterAttribute: String, type : String): QuerySnapshot {
        val date_index = if(filterAttribute == "date_month") {
            0
        } else {
            1
        }
        val mothAndDay = getMonthgetDay(null)
        val snapshot: QuerySnapshot = FirebaseFirestore
            .getInstance()
            .collection("ranking")
            .whereEqualTo("rankType", type)
            .whereEqualTo(filterAttribute, mothAndDay[date_index])
            .get()
            .await()
        return snapshot
    }

    /**getUserId
     * Return la id del usuario actual
     */
    fun getUserId(): String {
        var userId: String = ""
        if (currentUserUid == null) {
            Log.d("DBRank", "add current id = null")
        } else {
            userId = currentUserUid
            Log.d("DBRank", "add current id = ${currentUserUid}")
        }
        return userId
    }

    /**getUserName
     * Mediante la id del usuario retorna el nombre del usuario
     */
    suspend fun getUserName(currentUserUid: String): String {
        val customer: Customer? = getCustomerWithId(currentUserUid)
        val name: String
        name = if (customer == null) {
            ""
        } else {
            customer.username
        }
        return name
    }

    /**getCustomerWithId
     * Mediante la id del usuario retorna el objeto customer de dicho usuario
     */
    private suspend fun getCustomerWithId(currentUserUid: String): Customer? {
        return try {
            userCollection.document(currentUserUid).get().await()?.toObject(Customer::class.java)
        } catch (e: Exception) {
            Log.e("DBRank", "Error getting customer with ID $currentUserUid", e)
            null
        }
    }
}