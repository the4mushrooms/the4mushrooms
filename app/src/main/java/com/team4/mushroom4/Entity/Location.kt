package com.team4.mushroom4.Entity

data class Location(
    val id: String,
    val zone: Zone,
    val type: String
    // Otros campos opcionales pueden ser añadidos aquí
) {


    data class Zone(
        val latitude: String,
        val longitude: String
    )
}
