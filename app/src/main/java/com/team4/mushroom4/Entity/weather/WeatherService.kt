package com.team4.mushroom4.Entity.weather

import retrofit2.http.Query
import retrofit2.http.GET

interface WeatherService {
    @GET("data/2.5/weather")
    suspend fun getWeather(
        @Query("q") city: String,
        @Query("appid") apiKey: String,
        @Query("units") units: String = "metric" // Para obtener la temperatura en Celsius
    ): WeatherResponse
}

