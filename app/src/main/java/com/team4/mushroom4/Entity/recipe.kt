package com.team4.mushroom4.Entity

data class Recipe(
    val id: String,
    val title: String,
    val description: String,
    val ingredients: Ingredients,
    val steps: List<Step>
) {
    data class Ingredients(
        val ingredient: Ingredient
    ) {
        data class Ingredient(
            val id: String,
            val title: String,
            val image: String,
            val measures: String,
            val description: String
        )
    }

    data class Step(
        val id: String,
        val title: String,
        val instructions: String
    )
}
