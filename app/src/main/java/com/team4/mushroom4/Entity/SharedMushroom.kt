package com.team4.mushroom4.Entity

data class SharedMushroom(
    val name: String,
    val description: String,
    val latitude: Double,
    val longitude: Double,
    val userId: String,
)