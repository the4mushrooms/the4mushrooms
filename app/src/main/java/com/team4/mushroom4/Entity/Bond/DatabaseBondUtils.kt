package com.team4.mushroom4.Entity.Bond

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.team4.mushroom4.Entity.Customer
import com.team4.mushroom4.screens.bonds.Friend
import kotlinx.coroutines.tasks.await

class DatabaseBondUtils {
    private val db = FirebaseFirestore.getInstance()
    private val bondCollection: CollectionReference = db.collection("bonds")
    private val userCollection: CollectionReference = db.collection("users")
    private val currentUserUid: String = FirebaseAuth.getInstance().currentUser?.uid ?: ""

    /**getCurrentUserUid
     * Retorna el usuario actual
     */
    fun getCurrentUserUid(): String {
        return currentUserUid
    }

    /**exist
     * Verifica que existe el document en bonds
     */
    fun exist(id: String, callback: (Boolean) -> Unit) {
        val docRef = bondCollection.document(id)
        docRef.get().addOnSuccessListener { document ->
            if (document.exists()) {
                callback(true)
            } else {
                callback(false)
            }
        }
            .addOnFailureListener { exception ->
                Log.e("Firebase", "get failed with ", exception)
                callback(false)
            }
    }

    /**add
     * Guardar objeto Bond
     */
    fun add(bond: Bond) {
        if (bond.owner == "") {
            bond.owner = currentUserUid
        }
        try {
            bondCollection.document(bond.owner).set(bond)
        } catch (e: Exception) {
            Log.e("DBBonds", "add error: $e")
        }
    }

    /**getOwnerBondSnapshot
     * Recoge el documento bonds del usuario actual
     */
    @RequiresApi(Build.VERSION_CODES.O)
    suspend fun getOwnerBondSnapshot(): DocumentSnapshot? {
        return try {
            bondCollection.document(currentUserUid).get().await()
        } catch (e: Exception) {
            Log.e("DBBonds", "Error getting customer with ID $currentUserUid", e)
            null
        }
    }

    /**getUserName
     * Mediante la id del usuario retorna el nombre del usuario
     */
    suspend fun getUserName(userid: String): String {
        val customer: Customer? = getCustomerWithId(userid)
        return if (customer == null) {
            ""
        } else {
            customer.username
        }
    }

    /**getCustomerWithId
     * Mediante la id del usuario retorna el objeto customer de dicho usuario
     */
    private suspend fun getCustomerWithId(userid: String): Customer? {
        return try {
            userCollection.document(userid).get().await()?.toObject(Customer::class.java)
        } catch (e: Exception) {
            Log.e("DBBonds", "Error getting customer with ID $userid", e)
            null
        }
    }

    /**addUserToBond
     * Agrega un nuevo usuario a friendship del usuario activo
     */
    @RequiresApi(Build.VERSION_CODES.O)
    suspend fun addUserToBond(customerId: String) {
        val snapshot = getOwnerBondSnapshot()
        if (snapshot != null && customerId != currentUserUid) {
            val bond = snapshot.toObject(Bond::class.java)
            if (bond != null) {
                val newBond: Bond = Bond()
                val friendshipList: MutableMap<String, Boolean> = bond.friendships.toMutableMap()
                if (!friendshipList.containsKey(customerId)) {
                    friendshipList[customerId] = false
                }
                newBond.owner = bond.owner
                newBond.friendships = friendshipList
                newBond.shareds = newBond.shareds
                bondCollection.document(bond.owner).set(newBond)
            }
        } else {
            Log.e("DBBonds", "Error addUserToBond with ID $customerId to $currentUserUid")
        }
    }

    /**changeFriendship
     * Modifica friendship del usuario activo y
     * crea (si es necesario) un documento para el usuario objetivo
     * agrega el usuario activo en sus shareds
     */
    fun changeFriendship(friend: Friend, newValue: Boolean) {
        bondCollection.document(currentUserUid).update(
            "friendships.${friend.id}", newValue
        )

        exist(friend.id) { exists ->
            if (!exists) {
                val bond: Bond = Bond()
                bond.owner = friend.id
                add(bond)
            }
        }

        if (newValue == true) {
            bondCollection.document(friend.id).update(
                "shareds", FieldValue.arrayUnion(currentUserUid)
            )
        } else {
            bondCollection.document(friend.id).update(
                "shareds", FieldValue.arrayRemove(currentUserUid)
            )
        }
    }

    /**deleteFriendship
     * Elimina un usario de la lista friendships y elimina el usuario activo de su lista shareds
     */
    fun deleteFriendship(friend: Friend) {
        bondCollection.document(currentUserUid).update(
            "friendships.${friend.id}", FieldValue.delete()
        )

        bondCollection.document(friend.id).update(
            "shareds", FieldValue.arrayRemove(currentUserUid)
        )
    }
}

