package com.team4.mushroom4.Entity.Restaurants

import com.google.firebase.firestore.GeoPoint

data class Restaurant(
    val name: String = "",
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val description: String = "",
    val type: String = "restaurant"
)
