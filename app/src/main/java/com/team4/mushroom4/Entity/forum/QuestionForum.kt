import com.google.firebase.auth.FirebaseUser
import com.team4.mushroom4.Entity.forum.Response

data class QuestionForum(
    val id: String = "",
    val title: String = "",
    val description: String? = null,
    val userId: String? = "",
    val creationDate: Long? = null, // Podría ser un Timestamp o una representación en milisegundos
)