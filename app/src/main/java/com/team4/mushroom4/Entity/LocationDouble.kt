package com.team4.mushroom4.Entity

data class LocationDouble(
    val latitude: Double,
    val longitude: Double

)
