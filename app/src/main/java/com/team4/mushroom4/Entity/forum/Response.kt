package com.team4.mushroom4.Entity.forum

data class Response(
    val id: String = "",
    val questionId: String = "",
    val text: String = "",
    val userId: String = "",
    val creationDate: Long = 0
)