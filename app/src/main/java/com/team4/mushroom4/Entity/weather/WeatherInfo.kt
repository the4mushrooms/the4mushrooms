package com.team4.mushroom4.Entity.weather

data class WeatherInfo(
    val city: String,
    val temperature: Double,
    val condition: String,
    val humidity: Int,
    val windSpeed: Double
)
data class WeatherResponse(
    val name: String, // Nombre de la ciudad
    val main: Main,
    val weather: List<Weather>,
    val wind: Wind
) {
    data class Main(
        val temp: Double,
        val humidity: Int
    )

    data class Weather(
        val main: String, // Descripción general del clima, como "Clear" o "Rain"
        val description: String // Descripción más detallada del clima
    )

    data class Wind(
        val speed: Double
    )
}