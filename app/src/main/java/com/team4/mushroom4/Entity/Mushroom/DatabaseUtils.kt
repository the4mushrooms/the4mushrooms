package com.team4.mushroom4.Entity.Mushroom

import android.content.ContentValues.TAG
import android.util.Log
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import kotlinx.coroutines.tasks.await

data class Plant(
    val alter_name: String,
    val description: String,
    val hatDiameter: String,
    val imageDefectPath: String,
    val images: List<String>,
    val locations: List<GeoPoint>,
    val name: String,
    val scientific_name: String,
    val season: String,
    val type: String
)

class DatabaseUtils {

    private val db = FirebaseFirestore.getInstance()
    private val plantsCollection: CollectionReference = db.collection("plants")
    private val mushroomsCollection: CollectionReference = db.collection("mushrooms")

    fun addPlant(plant: Mushroom, onSuccess: () -> Unit, onFailure: (Exception) -> Unit) {
        try {
            plantsCollection
                .add(plant)
            onSuccess()
        } catch (e: Exception) {
            onFailure(e)
        }
    }

    fun getPlantById(documentId: String, onSuccess: (Mushroom?) -> Unit, onFailure: (Exception) -> Unit) {
        plantsCollection
            .document(documentId)
            .get()
            .addOnSuccessListener { documentSnapshot ->
                val plant = documentSnapshot.toObject(Mushroom::class.java)
                onSuccess(plant)
            }
            .addOnFailureListener { e ->
                onFailure(e)
            }
    }

    fun updatePlant(documentId: String, updatedData: Map<String, Any>, onSuccess: () -> Unit, onFailure: (Exception) -> Unit) {
        plantsCollection
            .document(documentId)
            .update(updatedData)
            .addOnSuccessListener {
                onSuccess()
            }
            .addOnFailureListener { e ->
                onFailure(e)
            }
    }

    fun deletePlant(documentId: String, onSuccess: () -> Unit, onFailure: (Exception) -> Unit) {
        plantsCollection
            .document(documentId)
            .delete()
            .addOnSuccessListener {
                onSuccess()
            }
            .addOnFailureListener { e ->
                onFailure(e)
            }
    }

    fun getAllMushrooms() {
        val db = FirebaseFirestore.getInstance()
        db.collection("mushrooms")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    Log.d(TAG, "${document.id} => ${document.data}")
                }
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error obteniendo documentos.", exception)
            }
    }

    suspend fun getAllPlants(): List<Mushroom> {
        return try {
            val snapshot = plantsCollection.get().await()
            snapshot.toObjects(Mushroom::class.java)
        } catch (e: Exception) {
            Log.w(TAG, "Error obteniendo documentos.", e)
            emptyList()
        }
    }

    suspend fun getAllPlantNames(): List<String> {
        return try {
            val snapshot = plantsCollection.get().await()
            snapshot.documents.mapNotNull { it.getString("name") }
        } catch (e: Exception) {
            Log.w(TAG, "Error obteniendo nombres de plantas.", e)
            emptyList()
        }
    }

    suspend fun getAllMyMushrooms(): List<Mushroom> {
        return try {
            val snapshot = mushroomsCollection.get().await()
            snapshot.toObjects(Mushroom::class.java)
        } catch (e: Exception) {
            Log.w(TAG, "Error obteniendo documentos.", e)
            emptyList()
        }
    }

    suspend fun getMushroomsByUserId(userId: String): List<Mushroom> {
        val db = FirebaseFirestore.getInstance()
        val mushroomsCollection = db.collection("mushrooms")

        return try {
            val querySnapshot = mushroomsCollection.whereEqualTo("userId", userId).get().await()

            if (!querySnapshot.isEmpty) {
                // Convierte el QuerySnapshot en una lista de objetos Mushroom
                querySnapshot.toObjects(Mushroom::class.java)
            } else {
                emptyList() // Devuelve una lista vacía si no hay setas asociadas al usuario
            }
        } catch (e: Exception) {
            // Maneja cualquier excepción que pueda ocurrir al interactuar con Firestore
            Log.e("DatabaseUtils", "Error al obtener las setas del usuario", e)
            emptyList()
        }
    }


}


