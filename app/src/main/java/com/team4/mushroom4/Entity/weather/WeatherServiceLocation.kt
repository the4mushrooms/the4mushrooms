package com.team4.mushroom4.Entity.weather

import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherServiceLocation {
    @GET("data/2.5/weather")
    suspend fun getWeatherByCoordinates(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("appid") apiKey: String,
        @Query("units") units: String = "metric"
    ): WeatherResponse
}