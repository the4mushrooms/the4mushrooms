package com.team4.mushroom4.Entity.Mushroom

import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint

data class Mushroom(
    var id: String = "",
    val alter_name: String = "",
    val description: String = "",
    val hatDiameter: String = "",
    val imageDefectPath: String= "",
    val images: List<String> ? = null,
    val locations: List<GeoPoint> ? = null,
    val name: String = "",
    val scientific_name: String = "",
    val season: String = "",
    val type: String = "",
    val userId: String = "",
    val timestamp: Timestamp = Timestamp.now(),
    val toxicity: String = ""
)