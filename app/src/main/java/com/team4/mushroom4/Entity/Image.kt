package com.team4.mushroom4.Entity

data class Image(
    val id: String,
    val caption: String,
    val path: String,
    val type: String
    // Otros campos opcionales pueden ser añadidos aquí
)
