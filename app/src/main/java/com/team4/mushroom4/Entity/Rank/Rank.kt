package com.team4.mushroom4.Entity.Rank

import java.sql.Timestamp

class Rank(
    var user_id: String = "",
    var user_name: String = "",
    /** uploadDate : String
     *  Partes de TimeStamp están deprecated y no puedo usar LocalDateTime en Firebase
     *  Para filtrar uso date_dayForyear y date_month
     *  */
    var uploadDate : String = "",
    var date_month : Int = 0,
    var date_dayForyear : Int = 0,
    var correctAnsweredQuestions: Int = 0,
    var durationS: Int = 0,
    var rankType: String = "",
    var rankLvl: Int = 0,
    var score: Int = 0,
)