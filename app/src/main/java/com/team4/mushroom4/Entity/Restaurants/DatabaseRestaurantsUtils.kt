package com.team4.mushroom4.Entity.Restaurant

import android.content.ContentValues.TAG
import android.util.Log
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.team4.mushroom4.Entity.Restaurants.Restaurant
import kotlinx.coroutines.tasks.await

class DatabaseRestaurantsUtils {

    private val db = FirebaseFirestore.getInstance()

    suspend fun getRestaurants(): List<Restaurant> {
        return db.collection("restaurants")
            .get()
            .await()
            .toObjects(Restaurant::class.java)
    }
}


