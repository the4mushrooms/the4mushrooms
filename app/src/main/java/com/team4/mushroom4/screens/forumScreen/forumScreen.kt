package com.team4.mushroom4.screens.forumScreen

import QuestionForum
import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.FilterList
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextOverflow
import androidx.navigation.NavController
import com.google.firebase.auth.FirebaseAuth
import com.team4.mushroom4.Entity.forum.Response
import com.team4.mushroom4.screens.forumScreen.components.deleteQuestionFromFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.UUID
import com.team4.mushroom4.screens.forumScreen.components.loadAllQuestionsFromFirestore
import com.team4.mushroom4.screens.forumScreen.components.loadResponsesForQuestionFromFirestore
import com.team4.mushroom4.screens.forumScreen.components.loadUserNameFromFirestore
import com.team4.mushroom4.screens.forumScreen.components.saveQuestionToFirestore
import com.team4.mushroom4.screens.forumScreen.components.saveResponseToFirestore


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun ForumScreen(navController: NavController) {
    var questions by remember { mutableStateOf<List<QuestionForum>>(emptyList()) }
    var filteredQuestions by remember { mutableStateOf<List<QuestionForum>>(emptyList()) }
    var selectedQuestionId by remember { mutableStateOf<String?>(null) }
    var addingQuestion by remember { mutableStateOf(false) }
    var showingUserQuestions by remember { mutableStateOf(false) }
    var newQuestionTitle by remember { mutableStateOf("") }
    var newQuestionDescription by remember { mutableStateOf("") }
    val coroutineScope = rememberCoroutineScope()
    val currentUserUid = FirebaseAuth.getInstance().currentUser?.uid


    // Función para cargar las preguntas y actualizar la lista
    fun loadQuestionsAndUpdate() {
        coroutineScope.launch {
            questions = loadAllQuestionsFromFirestore()
            filteredQuestions = if (showingUserQuestions) {
                questions.filter { it.userId == currentUserUid }
            } else {
                questions
            }
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("Forum", color = Color.White) },
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(Icons.Filled.ArrowBack, contentDescription = "Back")
                    }
                },
                actions = {
                    IconButton(onClick = { addingQuestion = true }) {
                        Icon(Icons.Filled.Add, contentDescription = "Add Question")
                    }
                    // Botón para filtrar las preguntas del usuario
                    IconButton(onClick = {
                        showingUserQuestions = !showingUserQuestions
                        if (showingUserQuestions) {
                            // Filtra para mostrar solo las preguntas del usuario
                            filteredQuestions = questions.filter { it.userId == currentUserUid }
                        } else {
                            // Restablece para mostrar todas las preguntas
                            filteredQuestions = questions
                        }
                    }) {
                        Icon(Icons.Filled.FilterList, contentDescription = "My Questions")
                    }
                },
                backgroundColor = Color(0xFF6D4C41)
            )
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            if (addingQuestion) {
                AddQuestionForm(
                    onAddQuestion = { title, description ->
                        val newQuestion = QuestionForum(
                            id = UUID.randomUUID().toString(),
                            title = title,
                            description = description,
                            userId = currentUserUid,
                            creationDate = System.currentTimeMillis()
                        )
                        questions = questions + newQuestion
                        filteredQuestions = questions // Asegurarse de actualizar la lista filtrada también
                        addingQuestion = false
                        coroutineScope.launch {
                            saveQuestionToFirestore(newQuestion)
                        }
                    },
                    onCancel = { addingQuestion = false }
                )
            }

            Spacer(modifier = Modifier.height(16.dp))

            val currentQuestionsList = if (showingUserQuestions) filteredQuestions else questions // Determina qué lista mostrar

            if (currentQuestionsList.isEmpty()) {
                Text("No questions available", style = MaterialTheme.typography.subtitle1)
            } else {
                LazyColumn {
                    items(currentQuestionsList) { question ->
                        QuestionItem(
                            question = question,
                            isSelected = question.id == selectedQuestionId,
                            onQuestionClicked = { selectedQuestion ->
                                selectedQuestionId = if (selectedQuestionId == selectedQuestion.id) null else selectedQuestion.id
                            },
                            onDeleteQuestion = { questionId ->
                                coroutineScope.launch {
                                    deleteQuestionFromFirestore(questionId) {
                                        // Después de eliminar la pregunta, cargamos nuevamente las preguntas y actualizamos la lista
                                        loadQuestionsAndUpdate()
                                    }
                                }
                            }
                        )
                        Divider(color = Color.Gray)
                    }
                }
            }
        }
    }

    // Llamamos a la función para cargar las preguntas al inicializar
    LaunchedEffect(Unit) {
        loadQuestionsAndUpdate()
    }
}
@Composable
fun QuestionItem(
    question: QuestionForum,
    isSelected: Boolean,
    onQuestionClicked: (QuestionForum) -> Unit,
    onDeleteQuestion: (String) -> Unit // Agregamos este parámetro
) {
    val coroutineScope = rememberCoroutineScope()
    var responses by remember { mutableStateOf<List<Response>>(emptyList()) }
    var questionUserName by remember { mutableStateOf<String?>(null) }
    val currentUserUid = FirebaseAuth.getInstance().currentUser?.uid

    // Usar el ID de la pregunta como clave para LaunchedEffect asegura que se recargue
    // la información cuando la pregunta seleccionada cambie.
    LaunchedEffect(question.id) {
        coroutineScope.launch {
            questionUserName = withContext(Dispatchers.IO) {
                loadUserNameFromFirestore(question.userId ?: "")
            }
            responses = withContext(Dispatchers.IO) {
                loadResponsesForQuestionFromFirestore(question.id)
            }.sortedBy { it.creationDate } // Ordenar respuestas por fecha más reciente
        }
    }

    Column(
        modifier = Modifier
            .clickable { onQuestionClicked(question) }
            .padding(8.dp)
            .fillMaxWidth()
            .background(Color(0xFFF8E1A2)),
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            // Fecha de creación
            Text(
                text = formatDate(question.creationDate),
                style = MaterialTheme.typography.caption,
                color = Color.Gray
            )

            // Nombre del usuario
            Text(
                text = "User: ${questionUserName ?: "Unknown"}",
                style = MaterialTheme.typography.subtitle1,
                color = Color(0xFF005500)
            )
        }

        Spacer(modifier = Modifier.height(4.dp))

        Text(
            text = question.title,
            style = MaterialTheme.typography.subtitle1,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )

        Spacer(modifier = Modifier.height(4.dp))

        Text(
            text = question.description ?: "",
            style = MaterialTheme.typography.body2,
            maxLines = 3,
            overflow = TextOverflow.Ellipsis
        )

        // Botón de eliminación solo visible si la pregunta es del usuario actual
        if (currentUserUid == question.userId) {
            IconButton(
                onClick = {
                    // Llamar a la función para eliminar la pregunta
                    coroutineScope.launch {
                        onDeleteQuestion(question.id)
                    }
                }
            ) {
                Icon(Icons.Filled.Delete, contentDescription = "Delete Question")
            }
        }

        if (isSelected) {
            Spacer(modifier = Modifier.height(8.dp))

            responses.forEach { response ->
                ResponseItem(response = response)
            }

            // AddResponseButton with onUpdateResponses function passed
            AddResponseButton(
                questionId = question.id,
                onUpdateResponses = {
                    // Reload responses after adding a new response
                    coroutineScope.launch {
                        responses = loadResponsesForQuestionFromFirestore(question.id).sortedBy { it.creationDate }
                    }
                }
            )
        }
    }
}

fun formatDate(creationDate: Long?): String {
    val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault())
    return if (creationDate != null) {
        formatter.format(Date(creationDate))
    } else {
        "Unknown date"
    }
}
@Composable
fun ResponseItem(response: Response) {
    var responseUserName by remember { mutableStateOf<String?>(null) }

    LaunchedEffect(Unit) {
        // Launch coroutine to load user name of response from Firestore
        responseUserName = withContext(Dispatchers.IO) {
            loadUserNameFromFirestore(response.userId)
        }
    }

    Column(
        modifier = Modifier
            .padding(start = 16.dp, top = 8.dp, bottom = 8.dp)
            .background(Color(0xFFE0F7FA)),
    ) {
        Text(
            text = "User: ${responseUserName ?: "Unknown"}",
            style = MaterialTheme.typography.subtitle1,
            color = Color(0xFF00796B) // Color para el nombre del usuario que hace la respuesta
        )

        Spacer(modifier = Modifier.height(4.dp))

        Text(
            text = "- " + response.text,
            style = MaterialTheme.typography.body2,
            color = Color.Gray
        )

        Spacer(modifier = Modifier.height(4.dp))
    }
}


@Composable
fun AddQuestionForm(
    onAddQuestion: (title: String, description: String) -> Unit,
    onCancel: () -> Unit
) {
    var title by remember { mutableStateOf("") }
    var description by remember { mutableStateOf("") }

    Column {
        TextField(
            value = title,
            onValueChange = { title = it },
            label = { Text("Title") },
            modifier = Modifier.fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(8.dp))

        TextField(
            value = description,
            onValueChange = { description = it },
            label = { Text("Description") },
            modifier = Modifier.fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(16.dp))

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.End
        ) {
            Button(onClick = { onAddQuestion(title, description) }) {
                Text("Add")
            }

            Spacer(modifier = Modifier.width(8.dp))

            Button(onClick = onCancel) {
                Text("Cancel")
            }
        }
    }
}






@Composable
fun AddResponseButton(questionId: String, onUpdateResponses: () -> Unit) {
    val coroutineScope = rememberCoroutineScope()
    var responseText by remember { mutableStateOf("") }
    var addingResponse by remember { mutableStateOf(false) }

    if (!addingResponse) {
        Button(onClick = { addingResponse = true }) {
            Text("Add Response")
        }
    }

    if (addingResponse) {
        TextField(
            value = responseText,
            onValueChange = { responseText = it },
            label = { Text("Your response") },
            modifier = Modifier.fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(8.dp))

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.End
        ) {
            Button(onClick = {
                if (responseText.isNotBlank()) {
                    val newResponse = Response(
                        id = UUID.randomUUID().toString(),
                        questionId = questionId,
                        text = responseText,
                        userId = FirebaseAuth.getInstance().currentUser?.uid ?: "",
                        creationDate = System.currentTimeMillis()
                    )
                    coroutineScope.launch {
                        saveResponseToFirestore(newResponse)
                    }
                    addingResponse = false
                    responseText = ""

                    // Call the function to update responses after adding a new response
                    onUpdateResponses()
                }
            }) {
                Text("Submit Response")
            }

            Spacer(modifier = Modifier.width(8.dp))

            Button(onClick = { addingResponse = false }) {
                Text("Cancel")
            }
        }
    }
}