package com.team4.mushroom4.screens.learn.quiz

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Button
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import com.team4.mushroom4.Entity.Mushroom.Mushroom
import com.team4.mushroom4.Entity.Quiz.Question
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import androidx.compose.runtime.mutableStateOf
import com.team4.mushroom4.Entity.Rank.DatabaseRankUtils
import com.team4.mushroom4.screens.learn.quiz.components.GenerateQuiz
import com.team4.mushroom4.screens.learn.quiz.components.LvlDialog
import com.team4.mushroom4.screens.learn.quiz.components.OverDialog
import java.time.LocalDateTime

@RequiresApi(Build.VERSION_CODES.O)
data class RunQuizScreen(
    val navController: NavController,
    private var quiz: List<Question> = newQuizList(1), // Llama lista de preguntas nivel 1
    private var question: Question? = null, // Pregunta actual
    private var lvl: Int = 1, // Nivel inicial
    var type: String = "mushroom", // Tipo de plant a usar
    val startTime: LocalDateTime = LocalDateTime.now(), // Fecha de inició de cuestionario
    private var score: Int = 0, // SCORE
    private val timePerQuestion: Int = 25 // Tiempo por pregunta
) {
    @RequiresApi(Build.VERSION_CODES.O)
    @Composable
    fun QuizScreen() {
        val currentItemIndex = remember { mutableIntStateOf(0) } // Indice para iterar las preguntas de quiz
        //
        var showLvlUpDialog by remember { mutableStateOf(false) } // Control de popups
        var timeToGameOver by remember { mutableIntStateOf(timePerQuestion) } // Temporizador
        val currentUserName = "" // saveUserName()
        var answer: String = "" // Variable respuesta correcta
        var quizSize: Int = 1 // Variable Tamaño de quiz
        var imageRoute: String = "" // Variable ruta de imagen
        // Try&Catch sobre lista de preguntas rellena las variables con los valores o retorna null
        try {
            question = quiz.get(currentItemIndex.intValue)
            answer = getAnswerValue(question!!)
            quizSize = quiz.size
            imageRoute = question!!.answer_object.imageDefectPath
        } catch (e:Exception){
            question = null
        }
        val questionRoute: Int = question?.route ?: 0 // Numero de pregunta de lista quiz
        val questionDescription: String = question?.question ?: "" // Texto/Descripcion de pregunta
        val questionOptionsList: List<String> = question?.answer_options ?: emptyList() // // Lista de respuestas posibles

        val scope = rememberCoroutineScope()
        var selectedValue = "" // Variable valor respuesta seleccionada
        val onSelectionChanged: (String) -> Unit = {} // Control cambio de  seleccion
        var isButtonEnabledState by remember { mutableStateOf(false) } // Estado de boton continuar con el quiz
        var correct by remember { mutableStateOf(false) } // Estado de respuesta proporcionada

        LazyColumn (
            modifier = Modifier
                .padding(16.dp)
        ) {
            item {
                Column(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    //MENSAJES DE ALERTA: LvlUp & GameOver
                    AlertScreen(
                        timeToGameOver,
                        correct,
                        questionRoute,
                        quizSize,
                        showLvlUpDialog,
                        currentUserName
                    )
                    //Imagen y temporizador
                    Box(
                        modifier = Modifier
                            //.fillMaxWidth()
                            .height(250.dp)
                            .clip(RoundedCornerShape(8.dp)) // Esquinas redondeadas para la imagen
                    ) {
                        Image(
                            modifier = Modifier.fillMaxSize(),
                            painter = rememberAsyncImagePainter(imageRoute),
                            contentDescription = "quiz image",
                            contentScale = ContentScale.Crop
                        )
                        Text(
                            modifier = Modifier
                                .align(Alignment.Center)
                                .padding(50.dp),
                            color = Color.White.copy(alpha = 0.7f),
                            style = TextStyle(
                                fontSize = 74.sp, // Tamaño de letra
                                //fontWeight = FontWeight.Bold
                            ),
                            text = "$timeToGameOver"
                        )
                        //Logica de temporizador
                        if (timeToGameOver > 0) {
                            LaunchedEffect(key1 = timeToGameOver) {
                                while (timeToGameOver > 0) {
                                    if(showLvlUpDialog){
                                        delay(2000L)
                                        showLvlUpDialog = false
                                    }
                                    delay(1000L)
                                    timeToGameOver--
                                }
                                scope.launch {
                                    //Tiempo agotado END
                                    delay(1000L)
                                    timeToGameOver = 0
                                    correct = false
                                }
                            }
                        }
                    }
                    Spacer(modifier = Modifier.height(16.dp))
                    //Pregunta:
                    Text(text = "Question nº $questionRoute/$quizSize :\n$questionDescription")
                    TEST_DEMO(answer, selectedValue) //respuesta a button ? DEMO
                    // Opciones de respuesta:
                    questionOptionsList.forEach { item ->
                        Row(
                            modifier = Modifier.selectable(
                                selected = selectedValue == item,
                                onClick = {
                                    //selectedValue = item // DEMO Cambiar valor para verlo por pantalla
                                    isButtonEnabledState = true
                                    onSelectionChanged(item)
                                }
                            ),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            RadioButton(
                                selected = selectedValue == item,
                                onClick = {
                                    isButtonEnabledState = true
                                    selectedValue = item
                                    onSelectionChanged(item)
                                }
                            )
                            Text(item)
                        }
                    }
                }
            }
            item { Spacer(modifier = Modifier.height(16.dp)) }
            item {
                // Boton para proseguir, condicional: Erronea, Siguiente tramo, Siguiente pregunta
                Button(
                    enabled = isButtonEnabledState,
                    onClick = {
                        isButtonEnabledState = false
                        val nextIndex = (currentItemIndex.intValue + 1) % quizSize
                        if (selectedValue != answer) {
                            // Respuesta erronea END
                            correct = false
                            timeToGameOver = 0
                        } else if (nextIndex == 0) {
                            // Respuesta correcta siguiente nivel LVL UP
                            //Si se acaban las preguntas END
                            correct = true
                            timeToGameOver = nextLvl(timeToGameOver)
                            currentItemIndex.intValue = 0 //NextIndex is reseted
                            if (timeToGameOver != 0) {
                                showLvlUpDialog = true
                            }
                        } else {
                            // Respuesta correcta siguiente pregunta
                            correct = true
                            score += timeToGameOver
                            timeToGameOver = timePerQuestion
                            currentItemIndex.intValue = nextIndex
                            showLvlUpDialog = false
                        }
                    },
                    colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF795548)), // Botón de color marrón
                    modifier = Modifier.fillMaxWidth()
                ) {
                    val nextIndex = currentItemIndex.intValue + 2
                    var buttonText = "Go to $nextIndex/$quizSize"
                    if (nextIndex - 1 >= quizSize) {
                        buttonText = "Advance to the next level"
                    }
                    Text(
                        text = buttonText,
                        color = Color.White
                    )
                }
            }
        }
    }

    @Composable
    private fun AlertScreen(
        timeToGameOver: Int,
        correct: Boolean,
        questionRoute: Int,
        quizSize: Int,
        showLvlUpDialog: Boolean,
        currentUserName: String
    ){
        if(timeToGameOver <= 0 || quiz.isEmpty()) {
            val nQuestion = getNumberCorrectAnswers(questionRoute, correct)
            var messagePunctuation: String = "You reached \nLVL: $lvl\nCorrect answers on lvl$lvl: $nQuestion of $quizSize\n"
            if (quiz.isEmpty()) {
                messagePunctuation = "You reached the end of \nLVL: ${lvl -1}"
            }
            OverDialog(navController, messagePunctuation, score, startTime, lvl, type, nQuestion, currentUserName)
        } else if(showLvlUpDialog){
            LvlDialog(lvl)
        }
    }

    private fun nextLvl(timeToGameOver:Int):Int{
        score += timeToGameOver
        lvl += 1
        quiz = newQuizList(lvl)
        if(quiz.isEmpty()){ return 0 }
        return timePerQuestion
    }
}

fun newQuizList(lvl:Int): List<Question>{
    val type: String = "mushroom"
    return GenerateQuiz(lvl, type)
}

// Verifica numero de respuestas correctas
fun getNumberCorrectAnswers(activeQuestion: Int, correct:Boolean): Int {
    var nQuestion : Int = 0
    try {
        nQuestion = activeQuestion
        if (!correct && nQuestion > 0) {
            nQuestion -= 1
        }
    } catch (e: NumberFormatException) {
        Log.d("finishQuiz", "No se pudo convertir el String a un Int")
    }
    return nQuestion
}

// Recupera la respuesca correcta del objeto Mushroom para comparar
fun getAnswerValue(question: Question): String {
    val objectPlant : Mushroom = question.answer_object
    return when (question.answer_attribute) {
        "name" -> objectPlant.name
        "alter_name" -> objectPlant.alter_name
        "description" -> objectPlant.description
        "hatDiameter" -> objectPlant.hatDiameter
        "imageDefectPath" -> objectPlant.imageDefectPath
        "images" -> objectPlant.images.toString()
        "scientific_name" -> objectPlant.scientific_name
        "season" -> objectPlant.season
        "toxicity" -> objectPlant.toxicity
        else -> ""
    }
}

//DEMO muesta información variada para demostraciones (ej.respuesta correcta)
@Composable
fun TEST_DEMO(answer: String, selectedValue: String) {
    Text(text = "DEMO answer: $answer")
    //Text(text = "DEMO selectedAnswer: $selectedValue")
}

/*
@RequiresApi(Build.VERSION_CODES.O)
fun getcurrentTime(): LocalDateTime {
    /*return SimpleDateFormat(
        "HH:mm:ss",
        Locale.getDefault()
    ).format(Date(SystemClock.elapsedRealtime()))*/
    return LocalDateTime.now()
}*/
