package com.team4.mushroom4.screens.learn.lvlSelection.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController

@Composable
fun Roadmap(navController: NavController){
    Spacer(modifier = Modifier.height(16.dp))
    Button(
        onClick = {
            //navController.navigate(Screen.QuizScreen.route)
            navController.navigate("RunQuizScreen")
        },
        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF795548)), // Botón de color marrón
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            text = "Mushroom Trivia",
            color = Color.White
        )
    }
}