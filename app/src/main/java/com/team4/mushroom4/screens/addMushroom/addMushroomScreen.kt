import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Location
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Camera
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.team4.mushroom4.Entity.Mushroom.DatabaseUtils
import com.team4.mushroom4.Entity.Mushroom.Mushroom
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.io.ByteArrayOutputStream
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.UUID

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun AddMushroomScreen(navController: NavController) {
    val coroutineScope = rememberCoroutineScope()
    val db = FirebaseFirestore.getInstance()
    val storage = FirebaseStorage.getInstance().reference
    val storageReference = storage.child("mushroom_images") // Adjust the path as needed

    // Estados para los campos del formulario
    var description by remember { mutableStateOf("") }
    var downloadUrl by remember { mutableStateOf("") }
    val location = remember { mutableListOf<GeoPoint>() }
    val currentDateTime = LocalDateTime.now()
    val formattedDate = currentDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))

    // Estado para controlar la visibilidad del Snackbar de error
    var showErrorSnackbar by remember { mutableStateOf(false) }

    // Obtener la ID del usuario actualmente autenticado en Firebase Authentication
    val currentUserUid = FirebaseAuth.getInstance().currentUser?.uid

    // Estado para la imagen capturada
    var capturedImage by remember { mutableStateOf<ImageBitmap?>(null) }
    val context = LocalContext.current

    // Lista de nombres predefinidos
    var mushroomNames by remember { mutableStateOf<List<String>>(emptyList()) }
    var selectedName by remember { mutableStateOf<String?>(null) }

    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.StartActivityForResult()
    ) { result ->
        val bitmap = result.data?.extras?.get("data") as Bitmap
        capturedImage = bitmap.asImageBitmap()

        // Upload the image to Firebase Storage
        uploadImageToStorage(storageReference, bitmap) { uri ->
            downloadUrl = uri
        }
    }

    // Obtén la lista de nombres de las plantas
    LaunchedEffect(Unit) {
        mushroomNames = DatabaseUtils().getAllPlantNames()
        // Asigna el primer nombre como seleccionado por defecto
        selectedName = mushroomNames.firstOrNull()
    }

    // UI
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("Add Mushroom") },
                backgroundColor = Color(0xFF6D4C41),
                navigationIcon = {
                    IconButton(onClick = { navController.navigateUp() }) {
                        Icon(Icons.Filled.ArrowBack, "Back", tint = Color.White)
                    }
                }
            )
        },
        backgroundColor = Color(0xFFEDE7F6)
    ) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
                .padding(16.dp)
                .verticalScroll(rememberScrollState()), // Agrega un scroll vertical
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            // Menú desplegable para seleccionar el nombre de la seta
            Box(
                modifier = Modifier.fillMaxWidth(),
                contentAlignment = Alignment.Center
            ) {
                var expanded by remember { mutableStateOf(false) }

                // Texto seleccionado para mostrar el nombre actualmente seleccionado
                Text(
                    text = "Selected Mushroom: ${selectedName ?: "Select a Name"}",
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable { expanded = true }
                )

                // Menú desplegable
                DropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false },
                    modifier = Modifier.fillMaxWidth()
                ) {
                    mushroomNames.forEach { name ->
                        DropdownMenuItem(onClick = {
                            selectedName = name
                            expanded = false
                        }) {
                            Text(text = name)
                        }
                    }
                }
            }

            // Campo para la descripcion
            OutlinedTextField(
                value = description,
                onValueChange = { description = it },
                label = { Text("Description") },
                modifier = Modifier.fillMaxWidth()
            )

            // Mostrar fecha y hora actuales, no editable por el usuario
            Text("Found on: $formattedDate", style = MaterialTheme.typography.body1)

            // Recuadro para mostrar la fotografía
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f) // Relación de aspecto 1:1
                    .padding(8.dp)
                    .background(Color.LightGray), // Aquí se utiliza la propiedad background
                contentAlignment = Alignment.Center
            ) {
                if (capturedImage != null) {
                    Image(
                        bitmap = capturedImage!!,
                        contentDescription = "Captured Photo",
                        modifier = Modifier.fillMaxSize()
                    )
                } else {
                    Text("Photo Preview", style = MaterialTheme.typography.subtitle1)
                }
            }

            // Botón para abrir la cámara
            IconButton(
                onClick = {
                    // Agrega la lógica para obtener la ubicación antes de tomar la foto aquí
                    getLastLocation(context,
                        onSuccess = { locationResult ->
                            locationResult?.let {
                                // Agrega la ubicación a la lista
                                location.add(GeoPoint(it.latitude, it.longitude))

                                // Ahora que tienes la ubicación, puedes llamar a takePhoto
                                takePhoto(context, launcher, storageReference, location) { bitmap ->
                                    capturedImage = bitmap.asImageBitmap()
                                }
                            }
                        },
                        onFailure = {
                            // Manejar el caso en que no se pueda obtener la ubicación
                            Log.e("AddMushroomScreen", "Error getting location", it)
                            showErrorSnackbar = true
                        }
                    )
                },
                modifier = Modifier.fillMaxWidth(),
                content = {
                    Icon(Icons.Filled.Camera, contentDescription = "Take Photo")
                }
            )

            // Spacer para empujar el botón hacia arriba
            Spacer(modifier = Modifier.weight(1f))

            // Botón para guardar
            Button(
                onClick = {
                    if (selectedName != null && downloadUrl.isNotEmpty()) {
                        coroutineScope.launch {
                            val newMushroomId = UUID.randomUUID().toString()
                            val imagePath = "$newMushroomId.jpg"
                            val newMushroom = Mushroom(
                                id = newMushroomId,
                                name = selectedName ?: "",
                                description = description,
                                locations = location,
                                timestamp = Timestamp.now(),
                                userId = currentUserUid ?: "",
                                images = listOf(imagePath),
                                imageDefectPath = downloadUrl
                            )

                            try {
                                // Store Mushroom in Firestore
                                db.collection("mushrooms").document(newMushroomId).set(newMushroom).await()

                                // Update the image path in the Firestore document
                                db.collection("mushrooms").document(newMushroomId)
                                    .update("images", listOf(imagePath)).await()
                            } catch (e: Exception) {
                                // Handle the error
                                Log.e("AddMushroomScreen", "Error adding mushroom", e)
                            }
                            navController.navigate("myMushrooms")
                        }
                    } else {
                        showErrorSnackbar = true
                        coroutineScope.launch {
                            delay(2000)
                            showErrorSnackbar = false
                        }
                    }
                },
                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Add Mushroom")
            }

            // Snackbar de error
            if (showErrorSnackbar && (selectedName == null || downloadUrl.isEmpty())) {
                Snackbar(
                    modifier = Modifier.padding(8.dp),
                    action = {
                        Button(onClick = { showErrorSnackbar = false }) {
                            Text("OK")
                        }
                    }
                ) {
                    Text("Please select a mushroom name and capture a photo")
                }
            }
        }
    }
}

fun takePhoto(
    context: android.content.Context,
    launcher: ActivityResultLauncher<Intent>,
    storage: StorageReference,
    location: MutableList<GeoPoint>, // Modificar la firma para aceptar la ubicación
    onPhotoCaptured: (Bitmap) -> Unit
) {
    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    if (intent.resolveActivity(context.packageManager) != null) {
        launcher.launch(intent)
    }
}

fun uploadImageToStorage(
    storageReference: StorageReference,
    bitmap: Bitmap,
    onUploadComplete: (String) -> Unit
) {
    val imageId = UUID.randomUUID().toString() // Generate a unique ID for the image
    val imageRef = storageReference.child("$imageId.jpg")

    // Convert bitmap to bytes
    val baos = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
    val imageData = baos.toByteArray()

    // Upload image to Firebase Storage
    val uploadTask = imageRef.putBytes(imageData)
    uploadTask.addOnCompleteListener { task ->
        if (task.isSuccessful) {
            // Image uploaded successfully
            Log.d("AddMushroomScreen", "Image uploaded successfully")

            // Get the download URL
            imageRef.downloadUrl.addOnSuccessListener { uri ->
                // Call the callback with the download URL
                onUploadComplete(uri.toString())
            }.addOnFailureListener {
                // Handle the failure to get the download URL
                Log.e("AddMushroomScreen", "Error getting download URL", it)
            }
        } else {
            // Handle the error
            Log.e("AddMushroomScreen", "Error uploading image", task.exception)
        }
    }
}

fun getLastLocation(context: Context, onSuccess: (Location?) -> Unit, onFailure: (Exception) -> Unit) {
    // Declara la variable para el cliente de ubicación fusionada
    lateinit var fusedLocationClient: FusedLocationProviderClient

    fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

    try {
        // Verifica si la aplicación tiene permisos de ubicación
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.d("Location", "Permisos de ubicación concedidos")
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    // Se llamó con éxito, usa la ubicación para obtener la latitud y longitud
                    if (location != null) {
                        Log.d("Location", "Ubicación obtenida: lat=${location.latitude}, lon=${location.longitude}")
                        onSuccess(location)
                    } else {
                        Log.d("Location", "La ubicación es nula")
                        onSuccess(null)
                    }
                }
                .addOnFailureListener { e ->
                    // Trata el error
                    Log.e("Location", "Error al obtener la ubicación", e)
                    onFailure(e)
                }
        } else {
            Log.e("Location", "Permisos de ubicación no concedidos")
            // Considera llamar a onFailure aquí o manejar la situación de permisos faltantes
        }
    } catch (e: SecurityException) {
        // Maneja la excepción de seguridad
        Log.e("Location", "Excepción de seguridad al obtener la ubicación", e)
        onFailure(e)
    }
}

