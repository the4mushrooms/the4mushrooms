package com.team4.mushroom4.screens.bonds.compose

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.ContentAlpha
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun TopBar(
    showTextField: MutableState<Boolean>,
    searchText: String,
    navController: NavController,
    onSearchTextChange: (String) -> Unit
) {
    TopAppBar(
        modifier = Modifier
            .padding(bottom = 16.dp)
            .fillMaxWidth(),
        backgroundColor = Color(0xFF6D4C41),
        title = {
            Text(
                modifier = Modifier.padding(16.dp),
                color = Color.White,
                text = "Friend List"
            )
        },
        navigationIcon = {
            IconButton(
                modifier = Modifier.width(30.dp),
                onClick = { navController.popBackStack() }
            ) {
                Icon(
                    imageVector = Icons.Filled.ArrowBack,
                    contentDescription = "Back",
                    tint = Color.White
                )
            }
        },
        actions = {
            Box {
                Row {
                    if (showTextField.value) {
                        Row {
                            TextField(
                                value = searchText,
                                onValueChange = onSearchTextChange,
                                modifier = Modifier
                                    .widthIn(max = 260.dp)
                                    .height(56.dp), // Altura estándar para una barra de búsqueda
                                textStyle = LocalTextStyle.current.copy(color = MaterialTheme.colors.onSurface),
                                placeholder = { Text("Search for friend name...") },
                                singleLine = true,
                                leadingIcon = {
                                    Icon(
                                        imageVector = Icons.Filled.Search,
                                        contentDescription = "Search",
                                        tint = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium)
                                    )
                                },
                                colors = TextFieldDefaults.textFieldColors(
                                    backgroundColor = MaterialTheme.colors.surface,
                                    cursorColor = MaterialTheme.colors.onSurface,
                                    leadingIconColor = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium),
                                    placeholderColor = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium),
                                    focusedIndicatorColor = Color.Transparent, // Elimina el indicador de foco
                                    unfocusedIndicatorColor = Color.Transparent, // Elimina el indicador cuando no está enfocado
                                    disabledIndicatorColor = Color.Transparent
                                ),
                                shape = MaterialTheme.shapes.small // Bordes redondeados
                            )
                            IconButton(
                                modifier = Modifier
                                    .padding(10.dp)
                                    .width(30.dp),
                                onClick = {
                                    showTextField.value = false
                                }
                            ) {
                                Icon(
                                    imageVector = Icons.Filled.Close,
                                    contentDescription = "Close",
                                    tint = Color.White,
                                    modifier = Modifier.height(56.dp)
                                )
                            }
                        }
                    } else {
                        IconButton(
                            modifier = Modifier
                                .padding(10.dp)
                                .width(30.dp),
                            onClick = { showTextField.value = true }
                        ) {
                            Icon(
                                imageVector = Icons.Filled.Search,
                                contentDescription = "Search",
                                tint = Color.White
                            )
                        }
                        IconButton(
                            modifier = Modifier
                                .padding(10.dp)
                                .width(30.dp),
                            onClick = { navController.navigate("usersList") }
                        ) {
                            Icon(
                                imageVector = Icons.Filled.Add,
                                contentDescription = "Add",
                                tint = Color.White
                            )
                        }
                    }
                }
            }
        }
    )
}