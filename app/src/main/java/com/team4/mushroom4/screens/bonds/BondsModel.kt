package com.team4.mushroom4.screens.bonds

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.navigation.NavController
import com.google.firebase.firestore.DocumentSnapshot
import com.team4.mushroom4.Entity.Bond.DatabaseBondUtils
import com.team4.mushroom4.Entity.Bond.Bond
import com.team4.mushroom4.screens.bonds.compose.FriendsScreen
import com.team4.mushroom4.screens.bonds.compose.TopBar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

data class Friend(
    var name: String,
    var id: String,
    var benefit: Boolean
)

class BondsModel(
    val navController: NavController,
) {
    @SuppressLint("NewApi", "UnrememberedMutableState")
    @Composable
    fun FriendList(){
        val friendsState = remember { mutableStateListOf<Pair<Int, Friend>>() }
        val coroutineScope = rememberCoroutineScope()
        val showTextField = remember { mutableStateOf(false) }
        var searchText by remember { mutableStateOf("") }
        //
        val filteredFriendsList = derivedStateOf {
            if (searchText.isEmpty()) {
                friendsState// En caso de que esté vacio muesta la lista descargada
            } else if (!showTextField.value) {
                searchText = "" // Si cierro el textField vacía el contenido de este
                friendsState // Esta vacío por lo que muestra la lista
            } else {
                friendsState.filter {
                    it.second.name.contains(searchText, ignoreCase = true)
                }.toList().let { list -> mutableStateListOf<Pair<Int, Friend>>().apply { addAll(list) } }
            } // Crea una lista filtrada siempre que tenga cadenas que coïncidan con TextField
        }.value
        //
        val dBBond: DatabaseBondUtils = DatabaseBondUtils()
        LaunchedEffect("getFriendList") {
            withContext(Dispatchers.IO) {
                val snapshotBond: DocumentSnapshot? = dBBond.getOwnerBondSnapshot()
                if(snapshotBond == null || !snapshotBond.exists()) {
                    val voidBond: Bond = Bond()
                    dBBond.add(voidBond)
                } else {
                    val bond: Bond? = snapshotBond.toObject(Bond::class.java)
                    var i = 0
                    for (amigo in bond!!.friendships){
                        val id: String = amigo.key
                        val name: String = dBBond.getUserName(id)
                        friendsState.add(
                            i to Friend(
                                name = name,
                                id = id,
                                benefit = amigo.value
                            )
                        )
                        i++
                    }
                }
            }
        }
        //
        Column {
            TopBar(showTextField, searchText, navController, onSearchTextChange = { searchText = it })
            FriendsScreen(filteredFriendsList, coroutineScope)
        }
    }
}