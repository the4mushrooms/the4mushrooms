package com.team4.mushroom4.screens.login
import Brown
import TextFieldWithIcon
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Green
import androidx.compose.ui.text.input.PasswordVisualTransformation

import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.firestore
import com.team4.mushroom4.Entity.Customer
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

@Composable
fun RegisterScreen(navController: NavController) {
    var email by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var username by remember { mutableStateOf("") } // Mantén el nombre de usuario para uso en Firestore
    var showError by remember { mutableStateOf(false) }
    val coroutineScope = rememberCoroutineScope()

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Brush.verticalGradient(listOf(Brown, Green)))
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            // Campo de texto para el correo electrónico
            TextFieldWithIcon(
                value = email,
                onValueChange = { email = it },
                label = "Correo Electrónico",
                icon = Icons.Filled.Email // Cambia el icono si es necesario
            )

            Spacer(modifier = Modifier.height(8.dp))

            // Campo de texto para la contraseña
            TextFieldWithIcon(
                value = password,
                onValueChange = { password = it },
                label = "Contraseña",
                icon = Icons.Filled.Lock,
                visualTransformation = PasswordVisualTransformation()
            )

            Spacer(modifier = Modifier.height(8.dp))

            // Campo de texto para el nombre de usuario (opcional, para uso en Firestore)
            TextFieldWithIcon(
                value = username,
                onValueChange = { username = it },
                label = "Nombre de Usuario",
                icon = Icons.Filled.AccountCircle
            )

            Spacer(modifier = Modifier.height(16.dp))

            Button(
                onClick = {
                    coroutineScope.launch {
                        val success = registerUser(email, password, username)
                        if (success) {
                            navController.navigate("loginScreen") {
                                popUpTo("registerScreen") { inclusive = true }
                            }
                        } else {
                            showError = true
                        }
                    }
                },
                colors = ButtonDefaults.buttonColors(backgroundColor = Green),
                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Registrarse", color = Color.White)
            }

            if (showError) {
                Text("Error al registrarse. Intente nuevamente.", color = Color.Red)
            }
        }
    }
}

suspend fun registerUser(email: String, password: String, username: String): Boolean {
    val auth = FirebaseAuth.getInstance()

    return try {
        val result = auth.createUserWithEmailAndPassword(email, password).await()
        val userId = result.user?.uid

        if (userId != null) {
            val db = Firebase.firestore
            val newUser = hashMapOf(
                "username" to username,
                "email" to email
            )
            db.collection("users").document(userId).set(newUser).await()
        }
        true
    } catch (e: Exception) {
        Log.d("RegisterScreen", "Error al registrar usuario: $e")
        false
    }
}