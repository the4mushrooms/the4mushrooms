package com.team4.mushroom4.screens.userMushrooms.components

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.*
import androidx.compose.ui.graphics.*
import androidx.compose.ui.text.input.*
import androidx.compose.ui.unit.*
import androidx.navigation.NavHostController
import com.google.firebase.firestore.FirebaseFirestore
import com.team4.mushroom4.Entity.Mushroom.Mushroom
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import androidx.compose.material.Text
import androidx.compose.material.MaterialTheme
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import coil.compose.rememberAsyncImagePainter
import androidx.compose.foundation.rememberScrollState


@Composable
fun EditMushroomScreen(
    navController: NavHostController,
    userId: String,
    mushroomId: String
) {

    Surface(
        modifier = Modifier.fillMaxSize(),

        ) {
        var mushroom by remember { mutableStateOf<Mushroom?>(null) }
        var editedName by remember { mutableStateOf(TextFieldValue("")) }
        var editedScientificName by remember { mutableStateOf(TextFieldValue("")) }
        var editedDescription by remember { mutableStateOf(TextFieldValue("")) }
        var editedHatDiameter by remember { mutableStateOf(TextFieldValue("")) }
        var editedSeason by remember { mutableStateOf(TextFieldValue("")) }
        var editedType by remember { mutableStateOf(TextFieldValue("")) }
        var editedPhoto by remember { mutableStateOf(TextFieldValue("")) }

        val coroutineScope = rememberCoroutineScope()

        LaunchedEffect(key1 = userId, key2 = mushroomId) {
            val db = FirebaseFirestore.getInstance()
            val doc = db.collection("mushrooms").document(mushroomId).get().await()
            mushroom = doc.toObject(Mushroom::class.java)
            mushroom?.let {
                editedName = TextFieldValue(it.name)
                editedScientificName = TextFieldValue(it.scientific_name)
                editedDescription = TextFieldValue(it.description)
                editedHatDiameter = TextFieldValue(it.hatDiameter)
                editedSeason = TextFieldValue(it.season)
                editedType = TextFieldValue(it.type)
                editedPhoto = TextFieldValue(it.imageDefectPath)
            }
        }
        mushroom?.let { mushroom ->
            EditMushroomForm(
                editedName = editedName,
                onNameChange = { editedName = it },
                editedScientificName = editedScientificName,
                onScientificNameChange = { editedScientificName = it },
                editedDescription = editedDescription,
                onDescriptionChange = { editedDescription = it },
                editedHatDiameter = editedHatDiameter,
                onHatDiameterChange = { editedHatDiameter = it },
                editedSeason = editedSeason,
                onSeasonChange = { editedSeason = it },
                editedType = editedType,
                onTypeChange = { editedType = it },
                editedPhoto = editedPhoto,
                onPhotoChange = { editedPhoto = it },
                onSave = {
                    coroutineScope.launch {
                        val updatedMushroom = mushroom.copy(
                            name = editedName.text,
                            scientific_name = editedScientificName.text,
                            description = editedDescription.text,
                            hatDiameter = editedHatDiameter.text,
                            season = editedSeason.text,
                            type = editedType.text,
                            imageDefectPath = editedPhoto.text
                        )
                        updateMushroomInFirestore(updatedMushroom)
                        navController.popBackStack()
                    }
                }
            )
        } ?: run {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                Text(
                    "Error: Mushroom not found",
                    style = MaterialTheme.typography.h1
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditMushroomForm(
    editedName: TextFieldValue,
    onNameChange: (TextFieldValue) -> Unit,
    editedScientificName: TextFieldValue,
    onScientificNameChange: (TextFieldValue) -> Unit,
    editedDescription: TextFieldValue,
    onDescriptionChange: (TextFieldValue) -> Unit,
    editedHatDiameter: TextFieldValue,
    onHatDiameterChange: (TextFieldValue) -> Unit,
    editedSeason: TextFieldValue,
    onSeasonChange: (TextFieldValue) -> Unit,
    editedType: TextFieldValue,
    onTypeChange: (TextFieldValue) -> Unit,
    editedPhoto: TextFieldValue,
    onPhotoChange: (TextFieldValue) -> Unit,
    onSave: () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .verticalScroll(rememberScrollState()), // Permite el desplazamiento vertical si el contenido excede la pantalla
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        OutlinedTextField(
            value = editedName,
            onValueChange = onNameChange,
            label = { Text("Name") },
            singleLine = true,
            colors = TextFieldDefaults.outlinedTextFieldColors()
        )
        Spacer(modifier = Modifier.height(8.dp))
        OutlinedTextField(
            value = editedScientificName,
            onValueChange = onScientificNameChange,
            label = { Text("Scientific Name") },
            singleLine = true,
            colors = TextFieldDefaults.outlinedTextFieldColors()
        )
        Spacer(modifier = Modifier.height(8.dp))
        OutlinedTextField(
            value = editedDescription,
            onValueChange = onDescriptionChange,
            label = { Text("Description") },
            colors = TextFieldDefaults.outlinedTextFieldColors()
        )
        Spacer(modifier = Modifier.height(8.dp))
        OutlinedTextField(
            value = editedHatDiameter,
            onValueChange = onHatDiameterChange,
            label = { Text("Hat Diameter") },
            singleLine = true,
            colors = TextFieldDefaults.outlinedTextFieldColors()
        )
        Spacer(modifier = Modifier.height(8.dp))
        OutlinedTextField(
            value = editedSeason,
            onValueChange = onSeasonChange,
            label = { Text("Season") },
            singleLine = true,
            colors = TextFieldDefaults.outlinedTextFieldColors()
        )
        Spacer(modifier = Modifier.height(8.dp))
        OutlinedTextField(
            value = editedType,
            onValueChange = onTypeChange,
            label = { Text("Type") },
            singleLine = true,
            colors = TextFieldDefaults.outlinedTextFieldColors()
        )
        Spacer(modifier = Modifier.height(8.dp))
        Image(
            painter = rememberAsyncImagePainter(editedPhoto.text),
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .height(250.dp)
                .clip(RoundedCornerShape(8.dp)), // Adjust the corner shape as needed
            contentScale = ContentScale.Crop
        )
        Spacer(modifier = Modifier.height(16.dp))
        Spacer(modifier = Modifier.height(16.dp))
        Button(
            onClick = onSave,
            shape = RoundedCornerShape(8.dp),
        ) {
            Text("Save Changes", color = Color.White)
        }
    }
}

suspend fun updateMushroomInFirestore(mushroom: Mushroom) {
    val db = FirebaseFirestore.getInstance()
    db.collection("mushrooms").document(mushroom.id).set(mushroom).await()
}
