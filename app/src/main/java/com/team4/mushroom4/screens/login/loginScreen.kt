import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.google.firebase.Firebase
import com.google.firebase.firestore.firestore
import com.team4.mushroom4.Entity.Customer
import com.team4.mushroom4.R
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Lock
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.filled.Email
import androidx.compose.runtime.*
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.google.firebase.auth.FirebaseAuth
import com.team4.mushroom4.components.Navigation


// Define tus colores personalizados
val Brown = Color(0xFF795548)
val Green = Color(0xFF4CAF50)

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun LoginScreen(navController: NavController) {
    var email by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var showError by remember { mutableStateOf(false) }
    val coroutineScope = rememberCoroutineScope()

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(
                Brush.verticalGradient(
                    colors = listOf(Brown, Green)
                )
            )
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            if (showError) {
                Text(
                    "Error de login. Verifica tus credenciales.",
                    color = Color.Red,
                    modifier = Modifier.padding(bottom = 8.dp)
                )
            }

            TextFieldWithIcon(
                value = email,
                onValueChange = { email = it },
                label = "Correo Electrónico",
                icon = Icons.Filled.Email // Asegúrate de tener este icono o usar uno alternativo
            )

            Spacer(modifier = Modifier.height(8.dp))

            TextFieldWithIcon(
                value = password,
                onValueChange = { password = it },
                label = "Contraseña",
                icon = Icons.Filled.Lock,
                visualTransformation = PasswordVisualTransformation()
            )

            Spacer(modifier = Modifier.height(16.dp))

            Button(
                onClick = {
                    coroutineScope.launch {
                        val success = signInWithEmail(email, password)
                        if (success) {
                            showError = false
                            navController.navigate("buttonsContent") {
                                popUpTo("login") { inclusive = true }
                            }
                        } else {
                            Log.d("LoginScreen", "Error de login")
                            showError = true
                        }
                    }
                },
                colors = ButtonDefaults.buttonColors(backgroundColor = Green),
                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Iniciar Sesión", color = Color.White)
            }
            Button(
                onClick = {
                    // Navega a la pantalla de registro
                    navController.navigate("register")
                },
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.LightGray),
                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Registrarse", color = Color.White)
            }
        }
    }
}

suspend fun signInWithEmail(email: String, password: String): Boolean {
    val auth = FirebaseAuth.getInstance()
    return try {
        Log.d("LoginScreen", "Iniciando sesión con $email")
        auth.signInWithEmailAndPassword(email, password).await()
        true // Login exitoso
    } catch (e: Exception) {
        Log.d("LoginScreen", "Error de login", e)
        false // Login fallido
    }
}

@Composable
fun TextFieldWithIcon(
    value: String,
    onValueChange: (String) -> Unit,
    label: String,
    icon: ImageVector,
    visualTransformation: VisualTransformation = VisualTransformation.None,
) {
    OutlinedTextField(
        value = value,
        onValueChange = onValueChange,
        label = { Text(label) },
        singleLine = true,
        leadingIcon = { Icon(icon, contentDescription = label) },
        visualTransformation = visualTransformation,
        modifier = Modifier.fillMaxWidth()
    )
}
