package com.team4.mushroom4.screens.menu.map

import com.team4.mushroom4.screens.map.MapLogic
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavController
import com.team4.mushroom4.components.BackButton


@Composable
fun MapScreen(navController: NavController) {
    val context = LocalContext.current
    val mapLogic = remember { MapLogic(context) }
    var showSetas by remember { mutableStateOf(true) }
    var showRestaurantes by remember { mutableStateOf(true) }
    var showFriendsSetas by remember { mutableStateOf(true) }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 56.dp),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.height(16.dp))

            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            ) {
                AndroidView(
                    factory = { mapLogic.mMap },
                    modifier = Modifier.fillMaxSize(),
                    update = {
                        // Update the MapView if needed

                    }
                )
            }

            Spacer(modifier = Modifier.height(16.dp))

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                FilterButton("Setas") {
                    showSetas = !showSetas
                    // Lógica para filtrar setas
                    mapLogic.filterMarkers(showSetas, showRestaurantes, showFriendsSetas)
                }

                FilterButton("Restaurantes") {
                    showRestaurantes = !showRestaurantes
                    // Lógica para filtrar restaurantes
                    mapLogic.filterMarkers(showSetas, showRestaurantes, showFriendsSetas)
                }

                FilterButton("Friends") {
                    showFriendsSetas = !showFriendsSetas
                    mapLogic.filterMarkers(showSetas, showRestaurantes, showFriendsSetas)
                }
            }

            BackButton(navController = navController)
        }
    }
}

@Composable
fun FilterButton(text: String, onClick: () -> Unit) {
    Button(
        onClick = onClick,
        modifier = Modifier
            .padding(8.dp)
            .size(100.dp, 40.dp) // Ajusta el tamaño aquí según sea necesario
    ) {
        Text(
            text = text,
            maxLines = 1, // Limita el texto a una línea
            overflow = TextOverflow.Ellipsis, // Agrega puntos suspensivos si el texto es demasiado largo para ajustarse
            fontSize = 8.sp // Ajusta el tamaño de la fuente aquí según sea necesario
        )
    }
}

