package com.team4.mushroom4.screens.bonds.userList

import android.annotation.SuppressLint
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.navigation.NavController
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.team4.mushroom4.Entity.Bond.DatabaseBondUtils
import com.team4.mushroom4.Entity.Customer
import com.team4.mushroom4.screens.bonds.userList.compose.TopBar
import com.team4.mushroom4.screens.bonds.userList.compose.UserScreen
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserModel(
    val navController: NavController,
){
    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("UnrememberedMutableState", "MutableCollectionMutableState")
    @Composable
    fun UserList(){
        val coroutineScope = rememberCoroutineScope()
        val userState = remember { mutableStateListOf<Customer>() }
        var searchText by remember { mutableStateOf("") }
        val filteredCustomerList = derivedStateOf {
            if (searchText.isEmpty()) {
                userState.toList()
            } else {
                userState.filter {
                    it.username.contains(searchText, ignoreCase = true)
                }
            }
        }.value
        //
        val dBBond: DatabaseBondUtils = DatabaseBondUtils()
        LaunchedEffect("getFriendList") {
            withContext(Dispatchers.IO) {
                val currentUserId = dBBond.getCurrentUserUid()
                val db = FirebaseFirestore.getInstance()
                val userCollection: CollectionReference = db.collection("users")
                userCollection.get().addOnSuccessListener { querySnapshot ->
                    for (document in querySnapshot.documents) {
                        val documentId = document.id
                        if(documentId!= currentUserId){
                            val customer: Customer? = document.toObject(Customer::class.java)
                            if( customer != null){
                                customer.id = documentId
                                userState.add(customer)
                            }
                        }
                    }
                }
            }
        }
        //
        Column (
        ) {
            TopBar(searchText, navController, onSearchTextChange = { searchText = it })
            UserScreen(filteredCustomerList, coroutineScope, navController)
        }
    }
}