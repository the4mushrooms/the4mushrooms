package com.team4.mushroom4.screens.forumScreen.components

import QuestionForum
import android.util.Log
import androidx.compose.runtime.MutableState
import com.google.firebase.firestore.FirebaseFirestore
import com.team4.mushroom4.Entity.forum.Response
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext


// Instancia compartida de FirebaseFirestore
val firestoreInstance: FirebaseFirestore by lazy {
    FirebaseFirestore.getInstance()
}
suspend fun deleteQuestionFromFirestore(questionId: String, onUpdateQuestions: () -> Unit) {
    try {
        firestoreInstance.collection("questions").document(questionId).delete().await()
        // Después de eliminar la pregunta, actualizamos la lista de preguntas
        onUpdateQuestions()
    } catch (e: Exception) {
        // Manejar el error, por ejemplo, mostrando un mensaje al usuario
        Log.e("deleteQuestion", "Error al eliminar la pregunta: ${e.message}")
    }
}
suspend fun saveResponseToFirestore(response: Response) {
    try {
        firestoreInstance.collection("responses").document(response.id).set(response).await()
    } catch (e: Exception) {
        // Handle error
    }
}

suspend fun loadUserNameFromFirestore(userId: String): String? {
    try {
        val userDocument = firestoreInstance.collection("users").document(userId).get().await()
        return userDocument.getString("username") ?: "Unknown"
    } catch (e: Exception) {
        Log.e("Error", "Error fetching user name: ${e.message}")
        return null
    }
}

suspend fun loadResponsesForQuestionFromFirestore(questionId: String): List<Response> {
    val responses = mutableListOf<Response>()

    try {
        val querySnapshot = firestoreInstance.collection("responses")
            .whereEqualTo("questionId", questionId)
            .get()
            .await()
        for (document in querySnapshot.documents) {
            val response = document.toObject(Response::class.java)
            response?.let {
                responses.add(it)
            }
        }
    } catch (e: Exception) {
        Log.e("error", e.toString())
    }

    return responses
}

suspend fun loadAllQuestionsFromFirestore(): List<QuestionForum> {
    val questions = mutableListOf<QuestionForum>()

    try {
        val querySnapshot = firestoreInstance.collection("questions").get().await()
        for (document in querySnapshot.documents) {
            val question = document.toObject(QuestionForum::class.java)
            question?.let {
                questions.add(it)
            }
        }
    } catch (e: Exception) {
        // Handle error
    }

    return questions
}

suspend fun saveQuestionToFirestore(question: QuestionForum) {
    try {
        firestoreInstance.collection("questions").document(question.id).set(question).await()
    } catch (e: Exception) {
        // Handle error
    }
}