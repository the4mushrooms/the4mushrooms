package com.team4.mushroom4.screens.userMushrooms

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.team4.mushroom4.Entity.Mushroom.Mushroom
import com.team4.mushroom4.screens.addMushroom.components.DetailSection
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.util.UUID

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MushroomCrudScreen(navController: NavController) {

    var mushrooms by remember { mutableStateOf<List<Mushroom>>(emptyList()) }
    val coroutineScope = rememberCoroutineScope()
    var selectedMushroom by remember { mutableStateOf<Mushroom?>(null) }
    var showDialog by remember { mutableStateOf(false) }

    // Get current user ID from Firebase Authentication
    val currentUserUid = FirebaseAuth.getInstance().currentUser?.uid

    // Function to fetch mushrooms associated with the current user from Firestore
    LaunchedEffect(key1 = currentUserUid) {
        if (currentUserUid != null) {
            val db = FirebaseFirestore.getInstance()
            val mushroomsCollection = db.collection("mushrooms")
            val querySnapshot = mushroomsCollection.whereEqualTo("userId", currentUserUid).get().await()
            val fetchedMushrooms = querySnapshot.toObjects(Mushroom::class.java)
            mushrooms = fetchedMushrooms
        }
    }

    // Function to show the confirmation dialog
    fun showConfirmationDialog(mushroom: Mushroom) {
        showDialog = true
        selectedMushroom = mushroom
    }

    // Function to dismiss the confirmation dialog
    fun dismissConfirmationDialog() {
        showDialog = false
        selectedMushroom = null
    }

    // Function to handle mushroom deletion
    fun handleDeleteMushroom(mushroom: Mushroom) {
        coroutineScope.launch {
            deleteMushroom(mushroom)
            // After deletion, refresh the list
            val updatedMushrooms = mushrooms.toMutableList()
            updatedMushrooms.remove(mushroom)
            mushrooms = updatedMushrooms
            dismissConfirmationDialog()
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("My Mushroom") },
                backgroundColor = Color(0xFF6D4C41), // Color del fondo de la barra superior
                elevation = 0.dp, // Elimina la elevación para una apariencia más limpia
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(Icons.Default.ArrowBack, contentDescription = "Back")
                    }
                }
            )
        },
        backgroundColor = Color.Transparent, // Fondo transparente para que el color del texto sea visible
        content = {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)
            ) {
                // Display list of mushrooms
                MushroomList(
                    mushrooms = mushrooms,
                    modifier = Modifier.weight(1f),
                    onClick = { mushroom ->
                        selectedMushroom = mushroom
                    },
                    onEdit = { mushroom ->
                        // Handle edit action here, for example:
                        navController.navigate("editMushroomScreen/${mushroom.userId}/${mushroom.id}")
                    },
                    onDelete = { mushroom ->
                        // Handle delete action here, for example:
                        showConfirmationDialog(mushroom)
                    }
                )

                // Button to navigate to screen to add a new mushroom
                Spacer(modifier = Modifier.height(16.dp))
                Button(
                    onClick = {
                        navController.navigate("addMushroomScreen") // Navigate to screen to add a new mushroom
                    },
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text("Add Mushroom")
                }

                // Show selected mushroom details summary
                selectedMushroom?.let { mushroom ->
                    MushroomDetails(mushroom = mushroom)
                }

                // Confirmation dialog for mushroom deletion
                if (showDialog) {
                    AlertDialog(
                        onDismissRequest = {
                            dismissConfirmationDialog()
                        },
                        title = {
                            Text("Delete Mushroom")
                        },
                        text = {
                            Text("Are you sure you want to delete this mushroom?")
                        },
                        confirmButton = {
                            Button(
                                onClick = {
                                    handleDeleteMushroom(selectedMushroom!!)
                                }
                            ) {
                                Text("Yes")
                            }
                        },
                        dismissButton = {
                            Button(
                                onClick = {
                                    dismissConfirmationDialog()
                                }
                            ) {
                                Text("No")
                            }
                        }
                    )
                }
            }
        }
    )
}

suspend fun deleteMushroom(mushroom: Mushroom) {
    val db = FirebaseFirestore.getInstance()
    val mushroomsCollection = db.collection("mushrooms")
    mushroomsCollection.document(mushroom.id).delete().await()
}

suspend fun addMushroom(mushroom: Mushroom) {
    val db = FirebaseFirestore.getInstance()
    val mushroomsCollection = db.collection("mushrooms")

    // Generar una ID única para la seta
    val mushroomId = UUID.randomUUID().toString()

    // Asignar la ID a la seta
    mushroom.id = mushroomId

    // Guardar la seta en la base de datos
    mushroomsCollection.document(mushroomId).set(mushroom).await()
}

@Composable
fun MushroomList(
    mushrooms: List<Mushroom>,
    modifier: Modifier = Modifier,
    onClick: (Mushroom) -> Unit,
    onEdit: (Mushroom) -> Unit,
    onDelete: (Mushroom) -> Unit
) {
    LazyColumn(modifier = modifier) {
        items(mushrooms) { mushroom ->
            MushroomItem(
                mushroom = mushroom,
                onClick = onClick,
                onEdit = onEdit,
                onDelete = onDelete
            )
        }
    }
}

@Composable
fun MushroomItem(
    mushroom: Mushroom,
    onClick: (Mushroom) -> Unit,
    onEdit: (Mushroom) -> Unit,
    onDelete: (Mushroom) -> Unit

) {
    Card(

        backgroundColor = Color.White,
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clip(shape = RoundedCornerShape(8.dp)), // Esquinas redondeadas
        elevation = 4.dp // Sombra para dar efecto de elevación
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.clickable { onClick(mushroom) }.padding(8.dp)
        ) {
            Image(
                painter = rememberAsyncImagePainter(mushroom.imageDefectPath),
                contentDescription = null,
                modifier = Modifier
                    .size(88.dp)
                    .clip(RoundedCornerShape(4.dp)), // Esquinas redondeadas en la imagen
                contentScale = ContentScale.Crop
            )
            Column(modifier = Modifier.padding(start = 8.dp).weight(1f)) {
                Text(
                    text = mushroom.name,
                    fontWeight = FontWeight.Bold,
                    fontSize = 16.sp
                )
                Text(
                    text = "Description: ${mushroom.description}",
                    style = MaterialTheme.typography.body2,
                    fontSize = 14.sp
                )
                Text(
                    text = "Date: ${mushroom.timestamp.toDate()}",
                    style = MaterialTheme.typography.body2,
                    fontSize = 14.sp
                )
                Text(
                    text = "Location: ${mushroom.locations}",
                    style = MaterialTheme.typography.body2,
                    fontSize = 14.sp
                )
            }
            IconButton(onClick = { onEdit(mushroom) }) {
                Icon(
                    imageVector = Icons.Default.Edit,
                    contentDescription = "Edit Mushroom",
                    tint = Color.Gray
                )
            }
            IconButton(onClick = { onDelete(mushroom) }) {
                Icon(
                    imageVector = Icons.Default.Delete,
                    contentDescription = "Delete Mushroom",
                    tint = Color.Gray
                )
            }
        }
    }
}

@Composable
fun MushroomDetails(mushroom: Mushroom) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFFF0EAE2)) // Un fondo suave que complemente el tema de naturaleza
            .padding(16.dp)
            .verticalScroll(rememberScrollState()), // Permite el desplazamiento vertical si el contenido excede la pantalla
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = rememberAsyncImagePainter(mushroom.imageDefectPath),
            contentDescription = "${mushroom.name} image",
            modifier = Modifier
                .fillMaxWidth()
                .height(250.dp)
                .clip(RoundedCornerShape(8.dp)), // Esquinas redondeadas para la imagen
            contentScale = ContentScale.Crop
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = mushroom.name,
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            color = Color(0xFF4E342E) // Un color oscuro que contraste bien con el fondo
        )
        Text(
            text = "Scientific name: ${mushroom.scientific_name}",
            fontSize = 18.sp,
            color = Color.DarkGray,
            modifier = Modifier.padding(top = 8.dp)
        )
        Text(
            text = "Description",
            fontSize = 20.sp,
            fontWeight = FontWeight.Medium,
            modifier = Modifier.padding(vertical = 8.dp),
            color = Color(0xFF6D4C41) // Un marrón suave para los títulos de sección
        )
        Text(
            text = mushroom.description,
            fontSize = 16.sp,
            color = Color.DarkGray,
            modifier = Modifier.padding(bottom = 8.dp)
        )
        // Repite el patrón para otros detalles como el diámetro del sombrero, la temporada, y el tipo
        DetailSection(title = "Hat Diameter", detail = mushroom.hatDiameter)
        DetailSection(title = "Season", detail = mushroom.season)
        DetailSection(title = "Type", detail = mushroom.type)
    }
}
