package com.team4.mushroom4.screens.addMushroom.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.team4.mushroom4.Entity.Mushroom.Mushroom

@Composable
fun MushroomList(mushrooms: List<Mushroom>, modifier: Modifier = Modifier, onClick: (Mushroom) -> Unit) {
    LazyColumn(modifier = modifier) {
        items(mushrooms) { mushroom ->
            MushroomItem(mushroom = mushroom, onClick = {
                onClick(mushroom) // Invocar la función onClick pasada como parámetro
            })
        }
    }
}

@Composable
fun MushroomItem(mushroom: Mushroom, onClick: () -> Unit) {
    Card(
        backgroundColor = Color.White,
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clip(shape = RoundedCornerShape(8.dp)), // Esquinas redondeadas
        elevation = 4.dp // Sombra para dar efecto de elevación
    ) {
        Row(modifier = Modifier.clickable(onClick = onClick).padding(8.dp)) {
            Image(
                painter = rememberAsyncImagePainter(mushroom.imageDefectPath),
                contentDescription = null,
                modifier = Modifier
                    .size(88.dp)
                    .clip(RoundedCornerShape(4.dp)), // Esquinas redondeadas en la imagen
                contentScale = ContentScale.Crop
            )
            Column(modifier = Modifier.padding(start = 8.dp).align(Alignment.CenterVertically)) {
                Text(
                    text = mushroom.name,
                    fontWeight = FontWeight.Bold,
                    fontSize = 16.sp
                )
                Text(
                    text = mushroom.description,
                    style = MaterialTheme.typography.body2,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis
                )
            }
        }
    }
}


@Composable
fun MushroomDetails(mushroom: Mushroom) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFFF0EAE2)) // Un fondo suave que complemente el tema de naturaleza
            .padding(16.dp)
            .verticalScroll(rememberScrollState()), // Permite el desplazamiento vertical si el contenido excede la pantalla
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = rememberAsyncImagePainter(mushroom.imageDefectPath),
            contentDescription = "${mushroom.name} image",
            modifier = Modifier
                .fillMaxWidth()
                .height(250.dp)
                .clip(RoundedCornerShape(8.dp)), // Esquinas redondeadas para la imagen
            contentScale = ContentScale.Crop
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = mushroom.name,
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            color = Color(0xFF4E342E) // Un color oscuro que contraste bien con el fondo
        )
        Text(
            text = "Scientific name: ${mushroom.scientific_name}",
            fontSize = 18.sp,
            color = Color.DarkGray,
            modifier = Modifier.padding(top = 8.dp)
        )
        Text(
            text = "Description",
            fontSize = 20.sp,
            fontWeight = FontWeight.Medium,
            modifier = Modifier.padding(vertical = 8.dp),
            color = Color(0xFF6D4C41) // Un marrón suave para los títulos de sección
        )
        Text(
            text = mushroom.description,
            fontSize = 16.sp,
            color = Color.DarkGray,
            modifier = Modifier.padding(bottom = 8.dp)
        )
        // Repite el patrón para otros detalles como el diámetro del sombrero, la temporada, y el tipo
        DetailSection(title = "Hat Diameter", detail = mushroom.hatDiameter)
        DetailSection(title = "Season", detail = mushroom.season)
        DetailSection(title = "Type", detail = mushroom.type)
    }
}

@Composable
fun DetailSection(title: String, detail: String) {
    Text(
        text = "$title:",
        fontSize = 18.sp,
        fontWeight = FontWeight.Bold,
        color = Color(0xFF6D4C41), // Un color coherente para todos los títulos de secciones
        modifier = Modifier.padding(top = 8.dp)
    )
    Text(
        text = detail,
        fontSize = 16.sp,
        color = Color.DarkGray,
        modifier = Modifier.padding(bottom = 8.dp)
    )
}
