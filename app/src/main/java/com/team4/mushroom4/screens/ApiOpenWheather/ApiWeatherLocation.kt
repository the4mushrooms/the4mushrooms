package com.team4.mushroom4.screens.ApiOpenWheather

import com.team4.mushroom4.Entity.weather.WeatherInfo
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.team4.mushroom4.Entity.weather.WeatherServiceLocation
import android.Manifest
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import android.content.Context
import android.content.pm.PackageManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import android.location.Location
import android.util.Log
import androidx.core.content.ContextCompat

// Declara la variable para el cliente de ubicación fusionada
private lateinit var fusedLocationClient: FusedLocationProviderClient

fun getLastLocation(context: Context, onSuccess: (Location?) -> Unit, onFailure: (Exception) -> Unit) {
    fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

    try {
        // Verifica si la aplicación tiene permisos de ubicación
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.d("Location", "Permisos de ubicación concedidos")
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    // Se llamó con éxito, usa la ubicación para obtener la latitud y longitud
                    if (location != null) {
                        Log.d("Location", "Ubicación obtenida: lat=${location.latitude}, lon=${location.longitude}")
                        onSuccess(location)
                    } else {
                        Log.d("Location", "La ubicación es nula")
                        onSuccess(null)
                    }
                }
                .addOnFailureListener { e ->
                    // Trata el error
                    Log.e("Location", "Error al obtener la ubicación", e)
                    onFailure(e)
                }
        } else {
            Log.e("Location", "Permisos de ubicación no concedidos")
            // Considera llamar a onFailure aquí o manejar la situación de permisos faltantes
        }
    } catch (e: SecurityException) {
        // Maneja la excepción de seguridad
        Log.e("Location", "Excepción de seguridad al obtener la ubicación", e)
        onFailure(e)
    }
}

suspend fun fetchWeatherInfoByCoordinates(latitude: Double, longitude: Double, apiKey: String): WeatherInfo {
    val retrofit = Retrofit.Builder()
        .baseUrl("https://api.openweathermap.org/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val apiService = retrofit.create(WeatherServiceLocation::class.java)
    val response = apiService.getWeatherByCoordinates(latitude = latitude, longitude = longitude, apiKey = apiKey)

    return WeatherInfo(
        city = response.name,
        temperature = response.main.temp,
        condition = response.weather.firstOrNull()?.main ?: "Unknown",
        humidity = response.main.humidity,
        windSpeed = response.wind.speed
    )
}