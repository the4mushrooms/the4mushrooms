package com.team4.mushroom4.screens.learn.quiz.components

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.team4.mushroom4.Entity.Mushroom.Mushroom
import com.team4.mushroom4.Entity.Quiz.Question
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await

class AnswerOptions {
    lateinit private var listOfPlants: List<Mushroom>
    var lvl: Int = 0
    lateinit var type: String

    fun answerAtributeController(
        listOfQuestion: List<Question>,
        inputLvl: Int,
        inputType: String
    ): List<Question> {
        lvl = inputLvl
        type = inputType
        val listaNueva = mutableListOf<Question>()
        //
        callListOfQuiz()
        for (question: Question in listOfQuestion) {
            val listaDeOpciones: List<String> = when (question.answer_attribute) {
                "season" -> {
                    askSeason(question.answer_object.season)
                }

                "toxicity" -> {
                    askToxicity(question.answer_object.toxicity)
                }

                else -> {
                    askAnswer(question)
                }
            }
            question.setAnswerOptions(listaDeOpciones.shuffled())
            listaNueva.add(question)
        }
        //
        return listaNueva
    }

    fun askSeason(correctAnswer: String): List<String> {
        val listOfOptions = listOf<String>(
            "Verano a Otoño",
            "Primavera a Otoño",
            "Primavera a Verano",
            "Otoño",
            "Primavera",
            "Verano",
            "Todo el año"
        ).shuffled()
        return listOf(correctAnswer) + listOfOptions.filter { it != correctAnswer }.take(2)
    }

    fun askToxicity(correctAnswer: String): List<String> {
        return listOf<String>("Toxico / No comestible", "Comestible")
    }

    fun askAnswer(question: Question): List<String> {
        val opcion : List<String> = when (question.answer_attribute) {
            "alter_name" -> {
                val listOfOptions : List<String> = listOfPlants.map { it.alter_name }
                val correctAnswer = question.answer_object.alter_name
                listOf(correctAnswer) + listOfOptions.filter { it != correctAnswer }.take(2)
            }
            "name" -> {
                val listOfOptions : List<String> = listOfPlants.map { it.name }
                val correctAnswer = question.answer_object.name
                listOf(correctAnswer) + listOfOptions.filter { it != correctAnswer }.take(2)
            }
            "scientific_name" -> {
                val listOfOptions : List<String> = listOfPlants.map { it.scientific_name }
                val correctAnswer = question.answer_object.scientific_name
                listOf(correctAnswer) + listOfOptions.filter { it != correctAnswer }.take(2)
            }
            else -> {
                listOf("","","")
            }
        }
        return opcion
    }

    fun callListOfQuiz() = runBlocking {
        listOfPlants = getPlants(lvl,type).shuffled()
    }

    suspend fun getPlants(lvl: Int, type: String): List<Mushroom> {
        val db = FirebaseFirestore.getInstance()
        val plantsCollection: CollectionReference = db.collection("plants")
        val plantList = mutableListOf<Mushroom>()
        runBlocking {
            val snapshot = plantsCollection
                //.whereEqualTo("lvl", lvl)
                .whereEqualTo("type", type)
                .get()
                .await()
            for (document in snapshot) {
                val plant = document.toObject(Mushroom::class.java)
                plantList.add(plant)
            }
        }
        return plantList.toList()
    }

    /*
    //DEMO Sin acceso a la DB
    fun listOfOptionsDEMO(quiz: List<Question>): List<Question> {
        quiz[0].answer_options = listOf("Verano a otoño", "Primavera", "Otoño").shuffled()
        quiz[1].answer_options = listOf("Primavera a otoño", "Primavera", "Otoño").shuffled()
        quiz[2].answer_options = listOf("Tricholoma matsutake", "Coprinus comatus", "Amanita phalloides").shuffled()
        quiz[3].answer_options = listOf("Hypholoma fasciculare", "Coprinus comatus", "Amanita phalloides").shuffled()
        quiz[4].answer_options = listOf("Barbuda", "Rucula", "Enciam").shuffled()
        return quiz
    }
    */
}