package com.team4.mushroom4.screens.learn.rank

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.ContentAlpha
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.google.firebase.firestore.QueryDocumentSnapshot
import com.team4.mushroom4.Entity.Rank.DatabaseRankUtils
import com.team4.mushroom4.Entity.Rank.Rank
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun RankScreen(
    navController: NavController
) {
    val type = "mushroom"
    val dBRank: DatabaseRankUtils = DatabaseRankUtils()
    val rankListStateDay = remember { mutableStateListOf<Rank>() }
    val rankListStateMonth = remember { mutableStateListOf<Rank>() }
    var isVisibleDay by remember { mutableStateOf(true) }
    var isVisibleMoth by remember { mutableStateOf(false) }
    val checkedState = remember { mutableStateOf(true) }
    var rankingMessage by remember { mutableStateOf("Day Ranking") }
    //
    LaunchedEffect("getRankData") {
        withContext(Dispatchers.IO) {
            val snapshotDay = dBRank.getRankSnapshot("date_month", type)
            val snapshotMonth = dBRank.getRankSnapshot("date_dayForyear", type)
            if (!snapshotMonth.isEmpty) {
                for (document in snapshotMonth) {
                    rankListStateDay.add(rankingList(dBRank, document))
                }
            }
            if (!snapshotDay.isEmpty) {
                for (document in snapshotDay) {
                    rankListStateMonth.add(rankingList(dBRank, document))
                }
            }
        }
    }
    //
    if (checkedState.value) {
        rankingMessage = "Day Ranking"
        isVisibleDay = true
        isVisibleMoth = false
    } else if (!checkedState.value) {
        rankingMessage = "Month Ranking"
        isVisibleDay = false
        isVisibleMoth = true
    }
    //
    Column {
        TopAppBar(
            modifier = Modifier
                .padding(bottom = 16.dp)
                .fillMaxWidth(),
            title = {
                Text(
                    modifier = Modifier.padding(16.dp),
                    color = Color.White,
                    text = rankingMessage
                )
            },
            backgroundColor = Color(0xFF6D4C41),
            navigationIcon = {
                    IconButton(
                        modifier = Modifier.width(30.dp),
                        onClick = { navController.popBackStack() }
                    ) {
                        Icon(Icons.Filled.ArrowBack, "Back", tint = Color.White)
                    }
            },
            actions = {
                Box{
                    Switch(
                        colors = SwitchDefaults.colors(
                            checkedTrackColor = Color(0xFFD2B48C),
                            disabledCheckedThumbColor = Color(0xFFD2B48C).copy(alpha = ContentAlpha.disabled),
                            disabledUncheckedThumbColor = Color(0xFFD2B48C).copy(alpha = ContentAlpha.disabled),
                        ),
                        checked = checkedState.value, // Bind the checked state to the Switch
                        onCheckedChange = { isChecked ->
                            checkedState.value = isChecked // Update the checked state when the Switch is clicked
                        }
                    )
                }
            }
        )
        RankList(ranking = rankListStateDay, isVisible = isVisibleDay)
        RankList(ranking = rankListStateMonth, isVisible = isVisibleMoth)
    }
}

@Composable
fun RankList(ranking: SnapshotStateList<Rank>, isVisible:Boolean) {
    val rankings: List<Rank> = ranking
    val sortedRankings: List<Rank> = rankings.sortedByDescending { it.score }.take(10)
    val gridHeight = (sortedRankings.size * 112).dp
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState())
            .height(if (isVisible) gridHeight else 0.dp) //Muestra o esconde la lista
    ) {
        LazyVerticalGrid(
            columns = GridCells.Fixed(2),
            contentPadding = PaddingValues(8.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp),
            content = {
                items(sortedRankings) { rank ->
                    RankItem(ranking = rank)
                }
            }
        )
    }
}

@Composable
fun RankItem(ranking: Rank) {
    Card(
        backgroundColor = Color.White,
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clip(shape = RoundedCornerShape(8.dp)),
        elevation = 4.dp
    ) {
        Row(modifier = Modifier) {
            Box(
                modifier = Modifier
                    .padding(start = 8.dp)
                    .align(Alignment.CenterVertically)
            ) {
                Column(){
                    Text(
                        text = ranking.user_name,
                        fontWeight = FontWeight.Bold,
                        fontSize = 16.sp
                    )
                    Text(
                        text = "${ranking.rankType} lvl.${ranking.rankLvl}",
                        fontWeight = FontWeight.Bold,
                        fontSize = 16.sp
                    )
                    Text(
                        text = "Score: ${ranking.score}",
                        style = MaterialTheme.typography.body2,
                        maxLines = 2,
                        overflow = TextOverflow.Ellipsis
                    )
                }
            }
        }
    }
}

/*fun asignColor(i:Int): Color {
    return if(i == 0){
        Color.Yellow
    } else if (i == 1){
        Color.Gray
    } else if ( i == 2) {
        Color(0xFF6D4C41)
    } else {
        Color.White
    }
}*/

suspend fun rankingList (dBRank: DatabaseRankUtils, document: QueryDocumentSnapshot) : Rank {
    val ranked: Rank = document.toObject(Rank::class.java)
    val customer: String = dBRank.getUserName(ranked.user_id)
    ranked.user_name = customer
    return ranked
}