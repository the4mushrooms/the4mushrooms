package com.team4.mushroom4.screens.ApiOpenWheather

import android.content.Context
import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.team4.mushroom4.Entity.weather.WeatherInfo
import kotlinx.coroutines.launch
import androidx.compose.runtime.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.navigation.NavHostController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import com.team4.mushroom4.Entity.weather.WeatherService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.accompanist.permissions.PermissionState




suspend fun fetchWeatherInfo(city: String, apiKey: String): WeatherInfo {
    val retrofit = Retrofit.Builder()
        .baseUrl("https://api.openweathermap.org/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val apiService = retrofit.create(WeatherService::class.java)
    val response = apiService.getWeather(city = city, apiKey = apiKey)

    return WeatherInfo(
        city = response.name,
        temperature = response.main.temp,
        condition = response.weather.firstOrNull()?.main ?: "Unknown",
        humidity = response.main.humidity,
        windSpeed = response.wind.speed
    )

}
@Composable
fun WeatherEntryPoint(navController: NavHostController) {
    val context = LocalContext.current
    val apiKey = "81e56e9cee731cdb3c0d759f771c7958"
    val coroutineScope = rememberCoroutineScope()

    // Definición de colores personalizados
    val darkGreen = Color(0xFF388E3C) // Un tono de verde más oscuro
    val brown = Color(0xFF795548) // Un tono de marrón

    var city by remember { mutableStateOf("") }
    var weatherInfo by remember { mutableStateOf<WeatherInfo?>(null) }
    var isLoading by remember { mutableStateOf(false) }
    var errorMessage by remember { mutableStateOf("") }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("Consulta del Clima") },
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(Icons.Filled.ArrowBack, contentDescription = "Volver")
                    }
                },
                backgroundColor = darkGreen // Usando el verde más oscuro aquí
            )
        }
    ) { padding ->
        Column(
            modifier = Modifier
                .padding(padding)
                .padding(16.dp)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            TextField(
                value = city,
                onValueChange = { city = it },
                label = { Text("Ciudad") },
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(8.dp))
            Button(
                onClick = {
                    coroutineScope.launch {
                        isLoading = true
                        errorMessage = ""
                        try {
                            weatherInfo = fetchWeatherInfo(city, apiKey) // Asume que tienes una versión de fetchWeatherInfoByCoordinates que acepta el nombre de la ciudad.
                        } catch (e: Exception) {
                            errorMessage = "Error obteniendo el clima: ${e.message}"
                        } finally {
                            isLoading = false
                        }
                    }
                },
                colors = ButtonDefaults.buttonColors(backgroundColor = brown), // Botón de color marrón
                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Buscar ciudad", color = Color.White)
            }
            Spacer(modifier = Modifier.height(8.dp))
            Button(
                onClick = {
                    coroutineScope.launch {
                        isLoading = true
                        errorMessage = ""
                        getLastLocation(context,
                            onSuccess = { location ->
                                if (location != null) {
                                    coroutineScope.launch {
                                        try {
                                            weatherInfo = fetchWeatherInfoByCoordinates(location.latitude, location.longitude, apiKey)
                                        } catch (e: Exception) {
                                            errorMessage = "Error obteniendo el clima: ${e.message}"
                                        } finally {
                                            isLoading = false
                                        }
                                    }
                                } else {
                                    errorMessage = "Error obteniendo la ubicación"
                                    isLoading = false
                                }
                            },
                            onFailure = { exception ->
                                errorMessage = "Error obteniendo la ubicación: ${exception.message}"
                                isLoading = false
                            }
                        )
                    }
                },
                colors = ButtonDefaults.buttonColors(backgroundColor = brown), // Botón de color marrón
                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Obtener ubicación actual", color = Color.White)
            }
            Spacer(modifier = Modifier.height(16.dp))
            when {
                isLoading -> CircularProgressIndicator()
                weatherInfo != null -> WeatherScreen(weatherInfo = weatherInfo!!)
                errorMessage.isNotEmpty() -> Text(errorMessage, color = Color.Red)
            }
        }
    }
}

    @Composable
    fun WeatherScreen(weatherInfo: WeatherInfo) {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            Column(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = "Clima en ${weatherInfo.city}",
                    style = MaterialTheme.typography.h4,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(bottom = 8.dp)
                )
                Spacer(modifier = Modifier.height(16.dp))
                Card(
                    elevation = 4.dp,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Column(
                        modifier = Modifier.padding(16.dp)
                    ) {
                        // Icono removido aquí
                        Text(
                            text = "${weatherInfo.temperature}°C",
                            style = MaterialTheme.typography.h6,
                            fontWeight = FontWeight.Medium
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = "Condición: ${weatherInfo.condition}",
                            style = MaterialTheme.typography.body1
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = "Humedad: ${weatherInfo.humidity}%",
                            style = MaterialTheme.typography.body1
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = "Velocidad del viento: ${weatherInfo.windSpeed} km/h",
                            style = MaterialTheme.typography.body1
                        )
                    }
                }
            }
        }
    }


