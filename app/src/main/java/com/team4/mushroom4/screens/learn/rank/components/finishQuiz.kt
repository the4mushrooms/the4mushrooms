package com.team4.mushroom4.screens.learn.rank.components

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.navigation.NavController
import com.team4.mushroom4.Entity.Rank.DatabaseRankUtils
import com.team4.mushroom4.Entity.Rank.Rank
import java.time.Duration
import java.time.LocalDateTime

/**finishQuiz
 * Esta funcion recoge el tiempo de inicio y fin para calcular el tiempo utilizado para completar el quiz
 * Tambien señala el numero de preguntas respondidas
 * El nivel y el typo de quiz
 */
@RequiresApi(Build.VERSION_CODES.O)
fun endQuiz(
    navController: NavController,
    startTime: LocalDateTime,
    lvl: Int,
    type: String,
    nQuestion: Int,
    finalScore: Int,
    userName: String
){
    val finalTime = LocalDateTime.now()
    val duration: Duration = Duration.between(startTime, finalTime)
    val dateMonth  = getMonthgetDay(finalTime)[0]
    val dateDayForyear = getMonthgetDay(finalTime)[1]

    createNewRank(duration, finalTime, dateMonth, dateDayForyear,  lvl, type, nQuestion, finalScore, userName)
    navController.navigate("learn")
}

@RequiresApi(Build.VERSION_CODES.O)
fun createNewRank(
    duration: Duration,
    finalTime: LocalDateTime,
    dateMonth: Int,
    dateDayForyear: Int,
    lvl: Int,
    type: String,
    nQuestion: Int,
    finalScore: Int,
    userName: String
): Rank {
    val rankToAdd : Rank = Rank(
        user_name = userName,
        uploadDate = finalTime.toString(),
        date_month  = dateMonth,
        date_dayForyear = dateDayForyear,
        correctAnsweredQuestions = nQuestion,
        durationS = duration.seconds.toInt(),
        rankType = type,
        rankLvl = lvl,
        score = finalScore
    )
    addRank(rankToAdd)
    return rankToAdd
}

fun addRank(rankToAdd: Rank) {
    val DBRank = DatabaseRankUtils()
    DBRank.add(rankToAdd)
}