package com.team4.mushroom4.screens.map

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint

// Referencia a Firestore y colección
private val db = FirebaseFirestore.getInstance()
val bondCollection: CollectionReference = db.collection("bonds")
val mushroomsCollection: CollectionReference = db.collection("mushrooms")
val currentUserUid: String = FirebaseAuth.getInstance().currentUser?.uid ?: ""

// Función para obtener las ID de los usuarios que comparten setas
fun getShareds(callback: (List<String>) -> Unit) {
    val sharedList = mutableListOf<String>()

    // Obtener todos los documentos de bonos
    bondCollection.get()
        .addOnSuccessListener { documents ->
            for (document in documents) {
                val friendships = document.get("friendships") as? Map<String, Boolean>
                if (friendships != null && friendships[currentUserUid] == true) {
                    // El usuario actual está marcado como amigo en este documento
                    sharedList.add(document.id)
                }
            }
            // Una vez se han revisado todos los documentos, llamar al callback con la lista
            Log.d("getShareds", "Personas que te han compartido setas:")
            Log.d("getShareds", sharedList.toString())
            callback(sharedList)
        }
        .addOnFailureListener { exception ->
            Log.e("getShareds", "Error al obtener los documentos de bonos", exception)
            // En caso de error, llamar al callback con una lista vacía
            callback(emptyList())
        }
}

// Función para obtener las setas compartidas por un usuario específico
fun getSharedMushrooms(userId: String, callback: (List<Map<String, Any>>) -> Unit) {
    val sharedMushroomsListVoid: List<Map<String, Any>> = listOf()

    mushroomsCollection.whereEqualTo("userId", userId).get()
        .addOnSuccessListener { querySnapshot ->
            val sharedMushroomsList = mutableListOf<Map<String, Any>>()
            for (document in querySnapshot.documents) {
                val mushroomData = document.data
                if (mushroomData != null) {
                    sharedMushroomsList.add(mushroomData)
                }
            }
            callback(sharedMushroomsList)
        }
        .addOnFailureListener { exception ->
            Log.e("getSharedMushrooms", "Error getting shared mushrooms", exception)
            callback(sharedMushroomsListVoid)
        }
}
// Función para procesar las ID de los usuarios que comparten setas
fun processSharedUsersIds(mapLogic: MapLogic) {
    getShareds { shareds ->
        Log.d("getShareds", "Personas que te han compartido setas:")
        shareds.forEach { shared ->
            Log.d("getShareds", shared)
            getUserNameById(shared) { userName ->
                if (userName != null) {
                    Log.d("getShareds", "Setas compartidas por el usuario $userName (ID: $shared):")
                } else {
                    Log.e("processSharedUsersIds", "No se pudo obtener el nombre del usuario con ID $shared")
                    return@getUserNameById  // Salir de la lambda si no se puede obtener el nombre del usuario
                }

                getSharedMushrooms(shared) { mushrooms ->
                    mushrooms.forEach { mushroom ->
                        Log.d("getShareds", mushroom.toString())
                        // Obtener las coordenadas de la seta compartida desde la lista de ubicaciones
                        val locations = mushroom["locations"] as List<*>
                        if (locations.isNotEmpty()) {
                            val latitude = (locations[0] as GeoPoint).latitude
                            val longitude = (locations[0] as GeoPoint).longitude
                            val name = mushroom["name"] as String
                            val description = mushroom["description"] as String
                            // Llamar a la función addFriendMushroomMarker
                            mapLogic.addFriendMushroomMarker(latitude, longitude, name, description, userName)
                        } else {
                            Log.e("processSharedUsersIds", "No se encontraron ubicaciones para la seta compartida")
                        }
                    }
                }
            }
        }
    }
}

fun getUserNameById(userId: String, callback: (String?) -> Unit) {
    val db = FirebaseFirestore.getInstance()
    val userCollection = db.collection("users")

    userCollection.document(userId).get()
        .addOnSuccessListener { document ->
            if (document != null && document.exists()) {
                val userName = document.getString("username")
                callback(userName)
            } else {
                // El usuario no existe o el documento está vacío
                callback(null)
            }
        }
        .addOnFailureListener { exception ->
            // Error al obtener los datos del usuario
            callback(null)
        }
}