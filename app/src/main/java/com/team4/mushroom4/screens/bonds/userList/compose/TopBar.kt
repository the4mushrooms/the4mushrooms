package com.team4.mushroom4.screens.bonds.userList.compose

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.ContentAlpha
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun TopBar(searchText: String, navController: NavController, onSearchTextChange: (String) -> Unit) {
    TopAppBar(
        modifier = Modifier
            .padding(bottom = 16.dp)
            .fillMaxWidth(),
        backgroundColor = Color(0xFF6D4C41),
        title = {
            TextField(
                value = searchText,
                onValueChange = onSearchTextChange,
                modifier = Modifier
                    .fillMaxWidth()
                    .widthIn(max = 260.dp)
                    .height(56.dp), // Altura estándar para una barra de búsqueda
                textStyle = LocalTextStyle.current.copy(color = MaterialTheme.colors.onSurface),
                placeholder = { Text("Search for user name...") },
                singleLine = true,
                leadingIcon = {
                    Icon(
                        imageVector = Icons.Filled.Search,
                        contentDescription = "Search",
                        tint = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium)
                    )
                },
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = MaterialTheme.colors.surface,
                    cursorColor = MaterialTheme.colors.onSurface,
                    leadingIconColor = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium),
                    placeholderColor = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium),
                    focusedIndicatorColor = Color.Transparent, // Elimina el indicador de foco
                    unfocusedIndicatorColor = Color.Transparent, // Elimina el indicador cuando no está enfocado
                    disabledIndicatorColor = Color.Transparent
                ),
                shape = MaterialTheme.shapes.small // Bordes redondeados
            )
        },
        navigationIcon = {
            IconButton(
                modifier = Modifier.width(30.dp),
                onClick = { navController.popBackStack() }
            ) {
                Icon(
                    imageVector = Icons.Filled.ArrowBack,
                    contentDescription = "Back",
                    tint = Color.White
                )
            }
        },
        actions = {}
    )
}