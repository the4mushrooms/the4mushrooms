package com.team4.mushroom4.screens.learn.lvlSelection

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.team4.mushroom4.screens.learn.lvlSelection.components.Roadmap

@Composable
fun LvlSelectionMenu(navController: NavController){
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = "Antes de iniciar: \n · El temporizador del cuestionario inicia en el momento que selecciona el nivel.")
        Spacer(modifier = Modifier.height(16.dp))
        Roadmap(navController)
        Spacer(modifier = Modifier.height(16.dp))
        Button(
            onClick = { navController.navigate("ranking") },
            colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF795548)), // Botón de color marrón
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = "Show Ranking",
                color = Color.White
            )
        }
        Spacer(modifier = Modifier.height(16.dp))
        Button(
            onClick = { navController.navigate("buttonsContent") },
            colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF795548)), // Botón de color marrón
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = "Go back",
                color = Color.White
            )
        }
    }
}