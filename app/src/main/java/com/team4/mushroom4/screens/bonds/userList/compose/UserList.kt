package com.team4.mushroom4.screens.bonds.userList.compose

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.team4.mushroom4.Entity.Bond.DatabaseBondUtils
import com.team4.mushroom4.Entity.Customer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun UserScreen(
    customerList: List<Customer>,
    coroutineScope: CoroutineScope,
    navController: NavController
) {
    val gridHeight = (customerList.size * 112).dp
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .height(gridHeight) //Muestra o esconde la lista
    ) {
        items(customerList) { customer ->
            UserCard(
                customer = customer,
                coroutineScope = coroutineScope,
                navController = navController
            )
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun UserCard(customer: Customer, coroutineScope: CoroutineScope, navController: NavController) {
    Card(
        backgroundColor = Color.White,
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clip(shape = RoundedCornerShape(8.dp)),
        elevation = 4.dp,
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp)
                .clickable {
                    coroutineScope.launch {
                        val dBUser = DatabaseBondUtils()
                        dBUser.addUserToBond(customer.id)
                        withContext(Dispatchers.Main) {
                            navController.popBackStack()
                        }
                    }
                },
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                modifier = Modifier
                    .weight(1f)
                    .padding(start = 8.dp),
                text = customer.username
            )
        }
    }
}