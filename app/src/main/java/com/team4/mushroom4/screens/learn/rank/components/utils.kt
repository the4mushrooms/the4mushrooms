package com.team4.mushroom4.screens.learn.rank.components

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDateTime
import java.time.YearMonth

/** getMonthgetDay
 *  Recibe LocalDateTime y devuelve un desglosado en array de tipo Int
 *  el numero de mes, numero del dia (vs Año)
 *  */
@RequiresApi(Build.VERSION_CODES.O)
fun getMonthgetDay(currentDate: LocalDateTime?): Array<Int> {
    val workDate: LocalDateTime = if(currentDate == null) {
        LocalDateTime.now()
    } else {
        currentDate
    }
    /* Para obtener el número de semana en kotlin: */
    /* val weekFields = WeekFields.of(Locale.getDefault())
     * val weekOfYear = dateTime.get(weekFields.weekOfYear()) */
    val monthOfYear = YearMonth.from(workDate).monthValue
    val dayOfYear = workDate.dayOfYear
    return arrayOf(monthOfYear, dayOfYear)
}