package com.team4.mushroom4.screens.RestaurantsList

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.BottomSheetScaffold
import androidx.compose.material.ContentAlpha
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.rememberBottomSheetScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.google.firebase.firestore.FirebaseFirestore
import com.team4.mushroom4.Entity.Restaurants.Restaurant
import com.team4.mushroom4.screens.RestaurantsList.components.RestaurantDetails
import com.team4.mushroom4.screens.RestaurantsList.components.RestaurantList
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

@SuppressLint("UnrememberedMutableState")
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun RestaurantsScreen(navController: NavController) {
    val sheetState = rememberBottomSheetScaffoldState()
    var selectedRestaurant by remember { mutableStateOf<Restaurant?>(null) }
    val coroutineScope = rememberCoroutineScope()
    var searchQuery by remember { mutableStateOf("") }
    val allRestaurants = remember { mutableStateListOf<Restaurant>() }

    val filteredRestaurants = derivedStateOf {
        if (searchQuery.isEmpty()) {
            allRestaurants.toList()
        } else {
            allRestaurants.filter {
                it.name.contains(searchQuery, ignoreCase = true)
            }
        }
    }.value

    LaunchedEffect(key1 = true) {
        val db = FirebaseFirestore.getInstance()
        try {
            val result = db.collection("restaurants").get().await()
            val restaurantList = result.toObjects(Restaurant::class.java)
            allRestaurants.clear()
            allRestaurants.addAll(restaurantList)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    BottomSheetScaffold(
        scaffoldState = sheetState,
        topBar = {
            TopAppBar(
                title = {
                    // Personalización de la barra de búsqueda
                    TextField(
                        value = searchQuery,
                        onValueChange = { searchQuery = it },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(56.dp), // Altura estándar para una barra de búsqueda
                        textStyle = LocalTextStyle.current.copy(color = MaterialTheme.colors.onSurface),
                        placeholder = { Text("Search restaurants...") },
                        singleLine = true,
                        leadingIcon = {
                            Icon(
                                Icons.Filled.Search,
                                contentDescription = "Search",
                                tint = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium)
                            )
                        },
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = MaterialTheme.colors.surface,
                            cursorColor = MaterialTheme.colors.onSurface,
                            leadingIconColor = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium),
                            placeholderColor = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium),
                            focusedIndicatorColor = Color.Transparent, // Elimina el indicador de foco
                            unfocusedIndicatorColor = Color.Transparent, // Elimina el indicador cuando no está enfocado
                            disabledIndicatorColor = Color.Transparent
                        ),
                        shape = MaterialTheme.shapes.small // Bordes redondeados
                    )
                },
                backgroundColor = Color(0xFF6D4C41),
                navigationIcon = {
                    IconButton(onClick = { navController.navigateUp() }) {
                        Icon(Icons.Filled.ArrowBack, "Back", tint = Color.White)
                    }
                }
            )
        },
        backgroundColor = Color(0xFFEDE7F6), // Fondo más suave
        sheetContent = {
            selectedRestaurant?.let { restaurant ->
                RestaurantDetails(restaurant = restaurant, onNavigateToLocation = {
                    // Aquí debes navegar a la pantalla del mapa con la ubicación del restaurante
                    navController.navigate("map") {
                    }
                })
            }
        },
        sheetPeekHeight = 0.dp
    ) { innerPadding ->
        RestaurantList(
            restaurants = filteredRestaurants,
            modifier = Modifier.padding(innerPadding),
            onClick = { restaurant ->
                selectedRestaurant = restaurant
                coroutineScope.launch {
                    sheetState.bottomSheetState.expand()
                }
            })
    }
}
