import android.annotation.SuppressLint
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.text.font.FontWeight
import androidx.navigation.NavHostController
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.launch
import com.team4.mushroom4.Entity.Mushroom.Mushroom
import com.team4.mushroom4.screens.MushroomList.components.MushroomDetails
import com.team4.mushroom4.screens.MushroomList.components.MushroomList
import androidx.compose.material.icons.filled.Search
import androidx.navigation.NavController


@SuppressLint("UnrememberedMutableState")
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun MushroomsScreen(navController: NavController) {
    val sheetState = rememberBottomSheetScaffoldState()
    var selectedMushroom by remember { mutableStateOf<Mushroom?>(null) }
    val coroutineScope = rememberCoroutineScope()
    var searchQuery by remember { mutableStateOf("") }
    val allMushrooms = remember { mutableStateListOf<Mushroom>() }

    val filteredMushrooms = derivedStateOf {
        if (searchQuery.isEmpty()) {
            allMushrooms.toList()
        } else {
            allMushrooms.filter {
                it.name.contains(searchQuery, ignoreCase = true)
            }
        }
    }.value

    LaunchedEffect(key1 = true) {
        val db = FirebaseFirestore.getInstance()
        try {
            val result = db.collection("plants").get().await()
            val mushroomList = result.toObjects(Mushroom::class.java)
            allMushrooms.clear()
            allMushrooms.addAll(mushroomList)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    BottomSheetScaffold(
        scaffoldState = sheetState,
        topBar = {
            TopAppBar(
                title = {
                    // Personalización de la barra de búsqueda
                    TextField(
                        value = searchQuery,
                        onValueChange = { searchQuery = it },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(56.dp), // Altura estándar para una barra de búsqueda
                        textStyle = LocalTextStyle.current.copy(color = MaterialTheme.colors.onSurface),
                        placeholder = { Text("Search mushrooms...") },
                        singleLine = true,
                        leadingIcon = {
                            Icon(
                                Icons.Filled.Search,
                                contentDescription = "Search",
                                tint = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium)
                            )
                        },
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = MaterialTheme.colors.surface,
                            cursorColor = MaterialTheme.colors.onSurface,
                            leadingIconColor = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium),
                            placeholderColor = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium),
                            focusedIndicatorColor = Color.Transparent, // Elimina el indicador de foco
                            unfocusedIndicatorColor = Color.Transparent, // Elimina el indicador cuando no está enfocado
                            disabledIndicatorColor = Color.Transparent
                        ),
                        shape = MaterialTheme.shapes.small // Bordes redondeados
                    )
                },
                backgroundColor = Color(0xFF6D4C41),
                navigationIcon = {
                    IconButton(onClick = { navController.navigateUp() }) {
                        Icon(Icons.Filled.ArrowBack, "Back", tint = Color.White)
                    }
                }
            )

        },
        backgroundColor = Color(0xFFEDE7F6), // Fondo más suave
        sheetContent = {
            selectedMushroom?.let { mushroom ->
                MushroomDetails(mushroom = mushroom) // Asegúrate de que este composable esté correctamente implementado
            }
        },
        sheetPeekHeight = 0.dp
    ) { innerPadding ->
        MushroomList(
            mushrooms = filteredMushrooms,
            modifier = Modifier.padding(innerPadding),
            onClick = { mushroom ->
                selectedMushroom = mushroom
                coroutineScope.launch {
                    sheetState.bottomSheetState.expand()
                }
            })
    }
}
