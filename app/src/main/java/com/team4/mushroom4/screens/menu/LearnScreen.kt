package com.team4.mushroom4.screens.menu

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.team4.mushroom4.screens.learn.lvlSelection.LvlSelectionMenu

@Composable
fun LearnScreen(navController: NavController) {
    Box(
        modifier = Modifier.fillMaxSize(),
        //contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
        ) {
            Text("Learn")
            Spacer(modifier = Modifier.height(16.dp))
            LvlSelectionMenu(navController)
            //Spacer(modifier = Modifier.height(16.dp))
            //BackButton(navController = navController)
        }
    }
}