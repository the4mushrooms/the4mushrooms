package com.team4.mushroom4.screens.learn.quiz.components

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavController
import com.team4.mushroom4.screens.learn.rank.components.endQuiz
import java.time.LocalDateTime

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun OverDialog(
    navController : NavController,
    messagePunctuation: String,
    score: Int,
    startTime: LocalDateTime,
    lvl: Int,
    type: String,
    nQuestion: Int,
    currentUserName: String
){
    AlertDialog(
        onDismissRequest = { /*closeDialog*/ },
        title = {
            Box(modifier = Modifier.fillMaxWidth()) {
                Text(
                    modifier = Modifier.align(Alignment.Center),
                    text = ".: GAME OVER :.",
                    color = Color(0xFF795548)
                )
            }
        },
        text = {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(
                    modifier = Modifier.wrapContentWidth(Alignment.CenterHorizontally),
                    text = "$messagePunctuation\nWith the following score: $score",
                    color = Color(0xFF795548)
                )
            }
        },
        confirmButton = {
            CheckUploadNewRecord(
                navController,
                score,
                startTime,
                lvl,
                type,
                nQuestion,
                currentUserName
            )
        },
        dismissButton = {
            TextButton(
                onClick = {
                    navController.navigate("learn")
                }
            ) {
                Text(
                    text = "Dismiss and close",
                    color = Color(0xFF795548)
                )
            }
        }
    )
}

@Composable
fun LvlDialog(lvl:Int){
    AlertDialog(
        onDismissRequest = { /*closeDialog*/ },
        title = {
            Box(modifier = Modifier.fillMaxWidth()) {
                Text(
                    modifier = Modifier.align(Alignment.Center),
                    text = ".: LEVEL UP :.",
                    color = Color(0xFF795548)
                )
            }
        },
        text = {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Text(
                    modifier = Modifier.wrapContentWidth(Alignment.CenterHorizontally),
                    text = "You reached LVL: $lvl",
                    color = Color(0xFF795548)
                )
            }
        },
        confirmButton = {},
        dismissButton = {}
    )
}
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CheckUploadNewRecord(
    navController : NavController,
    score: Int,
    startTime: LocalDateTime,
    lvl: Int,
    type: String,
    nQuestion: Int,
    currentUserName: String
){
    TextButton(
        onClick = {
            endQuiz(
                navController = navController,
                startTime = startTime,
                lvl = lvl,
                type = type,
                nQuestion = nQuestion,
                finalScore = score,
                userName = currentUserName
            )
            navController.navigate("learn")
        }) {
        Text(
            text = "Add your new record",
            color = Color(0xFF795548)
        )
    }
}