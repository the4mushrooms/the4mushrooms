package com.team4.mushroom4.screens.learn.quiz.components

import com.team4.mushroom4.Entity.Quiz.DatabaseQuizUtils
import com.team4.mushroom4.Entity.Quiz.Question

fun GenerateQuiz(
    lvl : Int,
    type: String
): List<Question> {
    //
    val quizUtils = DatabaseQuizUtils()
    quizUtils.callListOfQuiz(lvl, type)
    var quizShuffled : List<Question> = quizUtils.list.shuffled().take(5)
    //
    val classOptions: AnswerOptions = AnswerOptions()
    quizShuffled = classOptions.answerAtributeController(quizShuffled,lvl,type)
    //
    var numberOfQuestions: Int = 1
    quizShuffled.forEach { question ->
        question.route = numberOfQuestions
        numberOfQuestions++
    }
    return quizShuffled
}

/*
//DEMO Sin acceso a la DB
fun DEMO_WithoutDB(lvl:String,type: String) : List<Question> {
    var quiz = listOf(
        Question(
            route = 1,
            title = "Question 1.shuffled",
            lvl = lvl,
            answer_attribute = "season",
            answer_id = "BE8setFzWkEYZfcd3TMS",
            question = "En que estación se puede encontrar esta seta?",
        ),
        Question(
            route = 2,
            title = "Question 2.shuffled",
            lvl = lvl,
            answer_attribute = "season",
            answer_id = "Kp98azTMg2ryXdiM5tiH",
            question = "En que estación se puede encontrar esta seta?",
        ),
        Question(
            route = 3,
            title = "Question 3.shuffled",
            lvl = lvl,
            answer_attribute = "name",
            answer_id = "imusidO6rQg4rHDjXUlP",
            question = "Cual es el nombre de la siguiente seta?",
        ),
        Question(
            route = 4,
            title = "Question 4.shuffled",
            lvl = lvl,
            answer_attribute = "name",
            answer_id = "6Gemvi4D4UQJJPluO8R7",
            question = "Cual es el nombre de la siguiente seta?",
        ),
        Question(
            route = 5,
            title = "Question 5.shuffled",
            lvl = lvl,
            answer_attribute = "alter_name",
            answer_id = "Kp98azTMg2ryXdiM5tiH",
            question = "Por cual otro nombre se conoce la siguiente seta?",
        ),
    )

    quiz[0].answer_object = Mushroom(
        alter_name = "Oronja verde",
        description = "Extremadamente tóxica, esta seta es responsable de la mayoría de las intoxicaciones mortales por hongos.",
        hatDiameter = "5 a 15 cm",
        imageDefectPath = "https://micologica-barakaldo.org/wp-content/uploads/2019/10/Amanita-phalloides_20061015_008.jpg",
        images = listOf("http://www.amanitacesarea.com/imagenes/amanita/phalloides5.jpg","http://www.amanitacesarea.com/imagenes/amanita/phalloides5.jpg"),
        locations = listOf(),
        name = "Amanita phalloides",
        scientific_name = "",
        season = "Verano a otoño",
        type = "mushroom"
    )
    quiz[1].answer_object = Mushroom(
        alter_name = "Barbuda",
        description = "",
        hatDiameter = "",
        imageDefectPath = "https://lacasadelassetas.com/blog/wp-content/uploads/2023/10/coprinus-comatus-03.jpg",
        images = listOf(),
        locations = listOf(),
        name = "",
        scientific_name = "",
        season = "Primavera a otoño",
        type = "mushroom"
    )
    quiz[2].answer_object = Mushroom(
        alter_name = "",
        description = "",
        hatDiameter = "",
        imageDefectPath = "https://lacasadelassetas.com/blog/wp-content/uploads/2023/09/tricholoma-matsutake-01.jpg",
        images = listOf(),
        locations = listOf(),
        name = "Tricholoma matsutake",
        scientific_name = "",
        season = "",
        type = "mushroom"
    )
    quiz[3].answer_object = Mushroom(
        alter_name = "",
        description = "",
        hatDiameter = "",
        imageDefectPath = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/2014_Hypholoma_fasciculare.jpg/1200px-2014_Hypholoma_fasciculare.jpg",
        images = listOf(),
        locations = listOf(),
        name = "Hypholoma fasciculare",
        scientific_name = "",
        season = "",
        type = "mushroom"
    )
    quiz[4].answer_object = Mushroom(
        alter_name = "Barbuda",
        description = "",
        hatDiameter = "",
        imageDefectPath = "https://lacasadelassetas.com/blog/wp-content/uploads/2023/10/coprinus-comatus-03.jpg",
        images = listOf(),
        locations = listOf(),
        name = "Coprinus comatus",
        scientific_name = "Coprinus comatus",
        season = "",
        type = "mushroom"
    )
    return quiz
}
*/
