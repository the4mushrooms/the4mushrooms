package com.team4.mushroom4.screens.bonds.compose

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ContentAlpha
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.team4.mushroom4.Entity.Bond.DatabaseBondUtils
import com.team4.mushroom4.screens.bonds.Friend
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
fun FriendsScreen(friends: SnapshotStateList<Pair<Int, Friend>>, coroutineScope: CoroutineScope) {
    val gridHeight = (friends.size * 112).dp
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .height(gridHeight)
    ) {
        items(friends) { friend ->
            FriendCard(
                friend = friend,
                friends = friends,
                coroutineScope = coroutineScope
            )
        }
    }
}

@Composable
fun FriendCard(
    friend: Pair<Int, Friend>,
    friends: SnapshotStateList<Pair<Int, Friend>>,
    coroutineScope: CoroutineScope
) {
    Card(
        backgroundColor = Color.White,
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clip(shape = RoundedCornerShape(8.dp)),
        elevation = 4.dp
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                modifier = Modifier
                    .weight(1f)
                    .padding(start = 8.dp),
                text = friend.second.name
            )
            Row {
                Switch(
                    colors = SwitchDefaults.colors(
                        checkedTrackColor = Color(0xFF6D4C41),
                        disabledCheckedThumbColor = Color(0xFF6D4C41).copy(alpha = ContentAlpha.disabled),
                        disabledUncheckedThumbColor = Color(0xFF6D4C41).copy(alpha = ContentAlpha.disabled),
                    ),
                    checked = friend.second.benefit,
                    onCheckedChange = { newValue ->
                        coroutineScope.launch {
                            val dBBond = DatabaseBondUtils()
                            dBBond.changeFriendship(friend.second, newValue)// cambia el valor DB
                            //
                            friend.second.benefit = newValue
                            val index = friends.indexOf(friend) // cambia el valor visualmente
                            if (index >= 0) {
                                friends.removeAt(index)
                                friends.add(index, friend)
                            }
                        }
                    },
                )
                IconButton(
                    onClick = {
                        coroutineScope.launch {
                            val dBBond = DatabaseBondUtils()
                            dBBond.deleteFriendship(friend.second) //elimina el valor DB
                            friends.remove(friend) // elimina el valor visualmente
                        }
                    }
                ) {
                    Icon(
                        imageVector = Icons.Filled.Delete,
                        contentDescription = "DeleteFriend",
                        tint = Color(0xFF6D4C41)
                    )
                }
            }
        }
    }
}