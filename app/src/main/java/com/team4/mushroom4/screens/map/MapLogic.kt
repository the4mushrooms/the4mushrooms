package com.team4.mushroom4.screens.map
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.team4.mushroom4.Entity.Mushroom.DatabaseUtils
import com.team4.mushroom4.Entity.Restaurant.DatabaseRestaurantsUtils
import com.team4.mushroom4.R
import kotlinx.coroutines.runBlocking
import org.osmdroid.api.IMapController
import org.osmdroid.config.Configuration
import org.osmdroid.events.MapListener
import org.osmdroid.events.ScrollEvent
import org.osmdroid.events.ZoomEvent
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.ItemizedIconOverlay
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.OverlayItem
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay

class MapLogic(private val context: Context) : MapListener {
    lateinit var mMap: MapView
    lateinit var controller: IMapController
    lateinit var mMyLocationOverlay: MyLocationNewOverlay
    private val markersOverlay: ItemizedIconOverlay<OverlayItem> =
        ItemizedIconOverlay<OverlayItem>(context, ArrayList(), null)
    private val setaMarkers: MutableList<Marker> = mutableListOf()
    private val restaurantMarkers: MutableList<Marker> = mutableListOf()
    private val friendMarkers: MutableList<Marker> = mutableListOf()
    private var showSetas = true
    private var showRestaurantes = true
    private var isMovingMap = false

    init {
        initializeMap()
        runBlocking {

            loadMarkersFromDatabase(currentUserUid)
            loadMarkersRestFromDatabase()
            processSharedUsersIds()
        }
    }

    private fun initializeMap() {
        Configuration.getInstance().load(
            context.applicationContext,
            context.getSharedPreferences(context.getString(R.string.app_name), MODE_PRIVATE)
        )
        mMap = MapView(context)
        mMap.setTileSource(TileSourceFactory.MAPNIK)
        mMap.setMultiTouchControls(true)
        mMyLocationOverlay = MyLocationNewOverlay(GpsMyLocationProvider(context), mMap)
        controller = mMap.controller
        mMyLocationOverlay.enableMyLocation()
        mMyLocationOverlay.enableFollowLocation()
        mMyLocationOverlay.isDrawAccuracyEnabled = true
        mMyLocationOverlay.runOnFirstFix {
            if (context is AppCompatActivity) {
                context.runOnUiThread {
                    controller.setCenter(mMyLocationOverlay.myLocation)
                    controller.animateTo(mMyLocationOverlay.myLocation)
                }
            } else {
                Log.e("TAG", "Context is not an AppCompatActivity")
            }
        }
        controller.setZoom(3.0)
        mMap.minZoomLevel = 3.0
        mMap.overlays.add(mMyLocationOverlay)
        mMap.overlays.add(markersOverlay)
        mMap.addMapListener(this)
    }

    fun filterMarkers(showSetas: Boolean, showRestaurantes: Boolean, showFriendsSetas: Boolean) {
        mMap.overlays.clear()
        Log.d("FilterFriends", "showFriendsSetas: $showFriendsSetas")

        if (showSetas) {
            mMap.overlays.addAll(setaMarkers)
        }

        if (showRestaurantes) {
            mMap.overlays.addAll(restaurantMarkers)
        }
        Log.e("FilterFriends", friendMarkers.toString())
        if (showFriendsSetas) {
            mMap.overlays.addAll(friendMarkers)
        }

        mMap.invalidate()
    }

    private fun addSetaMarker(marker: Marker) {
        setaMarkers.add(marker)
        if (showSetas) {
            mMap.overlays.add(marker)
        }
    }

    private fun addRestaurantMarker(marker: Marker) {
        restaurantMarkers.add(marker)
        if (showRestaurantes) {
            mMap.overlays.add(marker)
        }
    }

    private suspend fun loadMarkersRestFromDatabase() {
        val firebaseUtils = DatabaseRestaurantsUtils()
        val restaurants = firebaseUtils.getRestaurants()

        for (restaurant in restaurants) {
            val geoPoint = GeoPoint(restaurant.latitude, restaurant.longitude)
            val marker = Marker(mMap).apply {
                position = geoPoint
                title = "Restaurant: ${restaurant.name}"
                subDescription = restaurant.description

                val newWidth = 150
                val newHeight = 200
                val resources = context.resources

                val originalBitmap =
                    BitmapFactory.decodeResource(resources, R.drawable.marcador_restaurantes)
                val scaledBitmap =
                    Bitmap.createScaledBitmap(originalBitmap, newWidth, newHeight, false)

                icon = BitmapDrawable(resources, scaledBitmap)
                setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
            }

            addRestaurantMarker(marker)
        }

        mMap.invalidate()
    }

    private suspend fun loadMarkersFromDatabase(currentUserUid: String) {
        val databaseUtils = DatabaseUtils()
        val mushrooms = databaseUtils.getMushroomsByUserId(currentUserUid)

        for (mushroom in mushrooms) {
            if (mushroom.locations.isNullOrEmpty()) {
                Log.e("MapLogic", "La ubicación de ${mushroom.name} es nula o está vacía")
                continue
            }

            val geoPoint = GeoPoint(mushroom.locations[0].latitude, mushroom.locations[0].longitude)
            Log.d("MapLogic", "Agregando marcador para ${mushroom.name} en $geoPoint")
            val marker = Marker(mMap).apply {
                position = geoPoint
                title = "Mushroom: ${mushroom.name}"
                subDescription = mushroom.description

                val newWidth = 150
                val newHeight = 200
                val resources = context.resources

                val originalBitmap =
                    BitmapFactory.decodeResource(resources, R.drawable.marcador_setas)
                val scaledBitmap =
                    Bitmap.createScaledBitmap(originalBitmap, newWidth, newHeight, false)

                icon = BitmapDrawable(resources, scaledBitmap)
                setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
            }

            addSetaMarker(marker)
        }

        mMap.invalidate()
    }
    override fun onScroll(event: ScrollEvent?): Boolean {
        if (!isMovingMap) {
            Log.e("TAG", "Latitude: ${event?.source?.mapCenter?.latitude}")
            Log.e("TAG", "Longitude: ${event?.source?.mapCenter?.longitude}")
        }
        return true
    }

    override fun onZoom(event: ZoomEvent?): Boolean {
        Log.e("TAG", "Zoom Level: ${event?.zoomLevel}")
        return false
    }

    fun addFriendMushroomMarker(
        latitude: Double,
        longitude: Double,
        name: String,
        description: String,
        userName: String
    ) {
        val geoPoint = GeoPoint(latitude, longitude)
        val marker = Marker(mMap).apply {
            position = geoPoint
            title = "Friend's Mushroom: $name"
            snippet = "Compartido por: $userName\n$description"

            val newWidth = 150
            val newHeight = 200
            val resources = context.resources

            // Cargar la nueva imagen del marcador
            val originalBitmap = BitmapFactory.decodeResource(resources, R.drawable.marcador_setas_compartida)
            val scaledBitmap = Bitmap.createScaledBitmap(originalBitmap, newWidth, newHeight, false)

            // Asignar la nueva imagen al marcador
            icon = BitmapDrawable(resources, scaledBitmap)
            setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        }

        // Agregar el marcador al mapa
        mMap.overlays.add(marker)
        friendMarkers.add(marker)
        mMap.invalidate()
    }


    // Función para procesar las ID de los usuarios que comparten setas
    fun processSharedUsersIds() {
        getShareds { shareds ->
            Log.d("getShareds", "Personas que te han compartido setas:")
            shareds.forEach { shared ->
                Log.d("getShareds", shared)
                getUserNameById(shared) { userName ->
                    if (userName != null) {
                        Log.d("getShareds", "Setas compartidas por el usuario $userName (ID: $shared):")
                    } else {
                        Log.e("processSharedUsersIds", "No se pudo obtener el nombre del usuario con ID $shared")
                        return@getUserNameById  // Salir de la lambda si no se puede obtener el nombre del usuario
                    }

                    getSharedMushrooms(shared) { mushrooms ->
                        mushrooms.forEach { mushroom ->
                            Log.d("getShareds", mushroom.toString())
                            // Obtener las coordenadas de la seta compartida desde la lista de ubicaciones
                            val locations = mushroom["locations"] as List<*>
                            if (locations.isNotEmpty()) {
                                val latitude = (locations[0] as com.google.firebase.firestore.GeoPoint).latitude
                                val longitude = (locations[0] as com.google.firebase.firestore.GeoPoint).longitude
                                val name = mushroom["name"] as String
                                val description = mushroom["description"] as String
                                // Llamar a la función addFriendMushroomMarker
                               addFriendMushroomMarker(latitude, longitude, name, description, userName)
                            } else {
                                Log.e("processSharedUsersIds", "No se encontraron ubicaciones para la seta compartida")
                            }
                        }
                    }
                }
            }
        }
    }
}